﻿using System.Data;
using System.Data.Entity;
using System.Linq;

namespace WebCompany.Data
{
    public class Repository<TObject> : IRepository<TObject> where TObject : class
    {
        protected Db Context = null;
        private bool shareContext = false;
        public Repository()
        {
            Context = new Db();
        }

        public Repository(Db context)
        {
            Context = context;
            shareContext = true;
        }

        protected DbSet<TObject> DbSet
        {
            get
            {
                return Context.Set<TObject>();
            }
        }

        public void Dispose()
        {
            if (shareContext == (Context != null))
                Context.Dispose();
        }
        public IQueryable<TObject> All()
        {
            return DbSet.AsQueryable();
        }

        public void Delete(TObject t)
        {
            var entry = Context.Entry(t);
            entry.State = EntityState.Deleted;
            this.Context.SaveChanges();
        }

        public int Update(TObject t)
        {
            var entry = Context.Entry(t);
            entry.State = EntityState.Modified;
            Context.SaveChanges();
            return 1;
        }

        public TObject Create(TObject t)
        {
            DbSet.Add(t);
            Context.SaveChanges();
            return t;
        } 

    }
}
