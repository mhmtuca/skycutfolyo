﻿using System;

namespace WebCompany.Data
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
    }
}
