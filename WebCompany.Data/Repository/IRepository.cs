﻿using System.Linq;

namespace WebCompany.Data
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> All();
        void Delete(T t);
        int Update(T t);
        T Create(T t);
    }
}
