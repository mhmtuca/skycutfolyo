﻿namespace WebCompany.Data
{
    public interface IDalContext : IUnitOfWork
    {
        ICategoryRepository CategoryData { get; }
        ISettingRepository SettingData { get; }
        //IMetaTagRepository MetaTagData { get; }
        IProductRepository ProductData { get; }
        ISearchKeywordsRepository SearchKeywordsData { get; } 
        ISliderRepository SliderData { get; }
        ITagRepository TagData { get; }
        IPagesRepository PagesData { get; } 
        IPageContentsRepository PageContentsData { get; }
        IMenuRepository MenuData { get; } 
        IGalleryRepository GalleryData { get; }
        IBlogRepository BlogData { get; }
        IMessageRepository MessageData { get; }
        IGalleryImageRepository GalleryImageData { get; }

        ILanguageRepository LanguageData { get; }

        IUrlRepository UrlData { get; }
        IVideoRepository VideoData { get; }
        IDocumentRepository DocumentData { get; }
    }
}
