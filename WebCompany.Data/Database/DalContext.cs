﻿using System;

namespace WebCompany.Data
{
    public class DalContext : IDalContext
    {
        private Db dbContext;
        private ICategoryRepository _CategoryData;
        private ISettingRepository _SettingData;
        //private IMetaTagRepository _MetaTagData;
        private IProductRepository _ProductData;
        private ISearchKeywordsRepository _SearchKeywordsData;
        private ISliderRepository _SliderData;
        private ITagRepository _TagData;
        private IPagesRepository _PagesData;
        private IPageContentsRepository _PageContentsData;
        private IMenuRepository _MenuData; 
        private IGalleryRepository _GalleryData;
        private IGalleryImageRepository _GalleryImageData;
        private IBlogRepository _BlogData;
        private IMessageRepository _MessageData;
        private IUrlRepository _UrlData;
        private ILanguageRepository _LanguageData;
        private IVideoRepository _VideoData;
        private IDocumentRepository _DocumentData;
        public DalContext()
        {
            dbContext = new Db();
        }
        public ICategoryRepository CategoryData
        {
            get
            {
                if (_CategoryData == null)
                    _CategoryData = new CategoryRepository(dbContext);
                return _CategoryData;
            }
        }

        public ISettingRepository SettingData
        {
            get
            {
                if (_SettingData == null)
                    _SettingData = new SettingRepository(dbContext);
                return _SettingData;
            }
        }
        public ILanguageRepository  LanguageData
        {
            get
            {
                if (_LanguageData == null)
                    _LanguageData = new LanguageRepository(dbContext);
                return _LanguageData;
            }
        }
        public IVideoRepository VideoData
        {
            get
            {
                if (_VideoData == null)
                    _VideoData = new VideoRepository(dbContext);
                return _VideoData;
            }
        }

        public IDocumentRepository DocumentData
        {
            get
            {
                if (_DocumentData == null)
                    _DocumentData = new DocumentRepository(dbContext);
                return _DocumentData;
            }
        }
        //public IMetaTagRepository MetaTagData
        //{
        //    get
        //    {
        //        if (_MetaTagData == null)
        //            _MetaTagData = new MetaTagRepository(dbContext);
        //        return _MetaTagData;
        //    }
        //}

        public IProductRepository ProductData
        {
            get
            {
                if (_ProductData == null)
                    _ProductData = new ProductRepository(dbContext);
                return _ProductData;
            }
        }

        public ISearchKeywordsRepository SearchKeywordsData
        {
            get
            {
                if (_SearchKeywordsData == null)
                    _SearchKeywordsData = new SearchKeywordsRepository(dbContext);
                return _SearchKeywordsData;
            }
        }

        public ISliderRepository SliderData
        {
            get
            {
                if (_SliderData == null)
                    _SliderData = new SliderRepository(dbContext);
                return _SliderData;
            }
        }

        public ITagRepository TagData
        {
            get
            {
                if (_TagData == null)
                    _TagData = new TagRepository(dbContext);
                return _TagData;
            }
        }

        public IPagesRepository PagesData
        {
            get
            {
                if (_PagesData == null)
                    _PagesData = new PagesRepository(dbContext);
                return _PagesData;
            }
        }

        public IMenuRepository MenuData
        {
            get
            {
                if (_MenuData == null)
                    _MenuData = new MenuRepository(dbContext);
                return _MenuData;
            }
        }
        
        public IGalleryRepository GalleryData
        {
            get
            {
                if (_GalleryData == null)
                    _GalleryData = new GalleryRepository(dbContext);
                return _GalleryData;
            }
        }

        public IGalleryImageRepository GalleryImageData
        {
            get
            {
                if (_GalleryImageData == null)
                    _GalleryImageData = new GalleryImageRepository(dbContext);
                return _GalleryImageData;
            }
        }
        public IBlogRepository BlogData
        {
            get
            {
                if (_BlogData == null)
                    _BlogData = new BlogRepository(dbContext);
                return _BlogData;
            }
        }

        public IMessageRepository MessageData
        {
            get
            {
                if (_MessageData == null)
                    _MessageData = new MessageRepository(dbContext);
                return _MessageData;
            }
        }

        public IPageContentsRepository PageContentsData
        {
            get
            {
                if (_PageContentsData == null)
                    _PageContentsData = new PageContentsRepository(dbContext);
                return _PageContentsData;
            }
        }
        public IUrlRepository UrlData
        {
            get
            {
                if (_UrlData == null)
                    _UrlData = new UrlRepository(dbContext);
                return _UrlData;
            }
        }
        public int SaveChanges()
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            if (CategoryData != null)
                CategoryData.Dispose();
            if (SettingData != null)
                SettingData.Dispose();
            //if (MetaTagData != null)
            //    MetaTagData.Dispose();
            if (ProductData != null)
                ProductData.Dispose();
            if (PagesData != null)
                PagesData.Dispose();
            if (PageContentsData != null)
                PageContentsData.Dispose();
            if (SearchKeywordsData != null)
                SearchKeywordsData.Dispose();
            if (SliderData != null)
                SliderData.Dispose();
            if (TagData != null)
                TagData.Dispose();
            if (MenuData != null)
                MenuData.Dispose();
            if (GalleryData != null)
                GalleryData.Dispose();
            if (GalleryImageData != null)
                GalleryImageData.Dispose();
            if (LanguageData != null)
                LanguageData.Dispose();
            if (BlogData != null)
                BlogData.Dispose();
            if (MessageData != null)
                MessageData.Dispose();
            if (UrlData != null)
                UrlData.Dispose();
            if (VideoData != null)
                VideoData.Dispose();
            if (DocumentData != null)
                DocumentData.Dispose();
            if (dbContext != null)
                dbContext.Dispose();
            
            GC.SuppressFinalize(this);
        }

        
    }
}
