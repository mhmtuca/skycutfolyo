﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Reflection;

namespace WebCompany.Data
{
    public class Db : DbContext
    {
        public Db(): base("WebContext")
        {
            Database.SetInitializer<Db>(null); 
        }

        public DbSet<CategoryDto> Category { get; set; }
        public DbSet<ProductsDto> Products { get; set; }
        //public DbSet<MetaTagDto> MetaTag { get; set; }
        public DbSet<TagsDto> Tags { get; set; }
        public DbSet<SearchKeywordsDto> SearchKeywords { get; set; }
        public DbSet<SettingDto> Setting { get; set; }
        public DbSet<SliderDto> Slider { get; set; }
        public DbSet<PagesDto> Pages { get; set; }
        public DbSet<MenuDto> Menu { get; set; } 
        public DbSet<GalleryDto> Gallery { get; set; }
        public DbSet<BlogDto> Blog { get; set; }
        public DbSet<MessageDto> Message { get; set; } 
        public DbSet<GalleryImageDto> GalleryImage { get; set; }
        public DbSet<VideoDto> Video { get; set; }
        public DbSet<DocumentDto> Dcoument { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
             
            var typesToRegister = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }
    }
}
