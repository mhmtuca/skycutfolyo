﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class SettingMap : EntityTypeConfiguration<SettingDto>
    {
        public SettingMap()
        {
            ToTable("Setting");
            HasKey(t => t.Id);
        }
    }
}
