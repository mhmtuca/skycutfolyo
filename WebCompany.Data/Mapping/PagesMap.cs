﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class PagesMap : EntityTypeConfiguration<PagesDto>
    {
        public PagesMap()
        {
            ToTable("Pages");
            HasKey(t => t.Id);
        }
    }
}
