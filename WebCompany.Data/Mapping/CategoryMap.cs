﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class CategoryMap : EntityTypeConfiguration<CategoryDto>
    {
        public CategoryMap()
        {
            ToTable("Category");
            HasKey(t => t.Id);

            this.HasRequired(m => m.CategoryLanguage)
       .WithMany(p => p.LanguageCategory)
       .HasForeignKey(m => m.LanguageId);
        }
    }
}
