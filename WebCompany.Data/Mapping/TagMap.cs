﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class TagMap : EntityTypeConfiguration<TagsDto>
    {
        public TagMap()
        {
            ToTable("Tags");
            HasKey(t => t.Id);
        }
    }
}
