﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data.Mapping
{
    public class MenuMap : EntityTypeConfiguration<MenuDto>
    {
        public MenuMap()
        {
            ToTable("Menu"); 
            HasKey(t => t.Id);

            this.HasRequired(m => m.MenuUrl)
          .WithMany(p => p.UrlMenu)
          .HasForeignKey(m => m.UrlId);
        }
    }
}
