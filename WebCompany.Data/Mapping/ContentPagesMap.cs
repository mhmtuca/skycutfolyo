﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class PageContentsMap : EntityTypeConfiguration<PageContentDto>
    {
        public PageContentsMap()
        {
            ToTable("PageContent");
            HasKey(t => t.Id);
        }
    }
}
