﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class DocumentMap : EntityTypeConfiguration<DocumentDto>
    {
        public DocumentMap()
        {
            ToTable("Documents");
            HasKey(t => t.Id);
        }
    }
}
