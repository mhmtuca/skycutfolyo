﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class BlogMap : EntityTypeConfiguration<BlogDto>
    {
        public BlogMap()
        {
            ToTable("Blog");
            HasKey(t => t.Id);
        }
    }
}
