﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class GalleryImageMap : EntityTypeConfiguration<GalleryImageDto>
    {
        public GalleryImageMap()
        {
            ToTable("GalleryImages");
            HasKey(t => t.Id);

         this.HasRequired(m => m.GalleryList)
         .WithMany(p => p.GalleryImageList)
         .HasForeignKey(m => m.GalleryId);
        }
    }
}
