﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class SearchKeywordsMap : EntityTypeConfiguration<SearchKeywordsDto>
    {
        public SearchKeywordsMap()
        {
            ToTable("SearchKeywords");
            HasKey(t => t.Id);
        } 
    }
}
