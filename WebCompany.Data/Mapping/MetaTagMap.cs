﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class MetaTagMap : EntityTypeConfiguration<MetaTagDto>
    {
        public MetaTagMap()
        {
            ToTable("MetaTag");
            HasKey(t => t.Id);
        }
    }
}
