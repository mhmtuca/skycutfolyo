﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class VideoMap : EntityTypeConfiguration<VideoDto>
    {
        public VideoMap()
        {
            ToTable("Videos");
            HasKey(t => t.Id);
        }
    }
}
