﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class LanguageMap : EntityTypeConfiguration<LanguageDto>
    {
        public LanguageMap()
        {
            ToTable("Language");
            HasKey(t => t.Id); 
       
        }
    }
}
