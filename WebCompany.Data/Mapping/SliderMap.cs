﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class SliderMap : EntityTypeConfiguration<SliderDto>
    {
        public SliderMap()
        {
            ToTable("Slider");
            HasKey(t => t.Id);
        }
    }
}
