﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Data
{
    public class MessageMap : EntityTypeConfiguration<MessageDto>
    {
        public MessageMap()
        {
            ToTable("Message");
            HasKey(t => t.Id);
        }
    }
}
