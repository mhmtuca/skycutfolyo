﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class GalleryMap : EntityTypeConfiguration<GalleryDto>
    {
        public GalleryMap()
        {
            ToTable("Gallery");
            HasKey(t => t.Id);
        }
    }
}
