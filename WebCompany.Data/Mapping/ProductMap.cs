﻿using System.Data.Entity.ModelConfiguration;

namespace WebCompany.Data
{
    public class ProductMap : EntityTypeConfiguration<ProductsDto>
    {
        public ProductMap()
        {
            ToTable("Products");
            HasKey(t => t.Id);

           this.HasRequired(m => m.ProductCategory)
          .WithMany(p => p.CategoryProducts)
          .HasForeignKey(m => m.CategoryId); 
        }
    }
}
