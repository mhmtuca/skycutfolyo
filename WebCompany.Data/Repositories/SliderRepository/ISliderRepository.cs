﻿namespace WebCompany.Data
{
    public interface ISliderRepository : IRepository<SliderDto>
    {
        string GetUrl();

        void Dispose();
    }
}

