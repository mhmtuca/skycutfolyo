﻿namespace WebCompany.Data
{
    public interface IMessageRepository : IRepository<MessageDto>
    {
        string GetUrl();

        void Dispose();
    }
}

