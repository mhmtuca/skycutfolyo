﻿namespace WebCompany.Data
{
    public interface ISettingRepository : IRepository<SettingDto>
    {
        string GetUrl();

        void Dispose();
    }
}

