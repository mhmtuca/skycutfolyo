﻿namespace WebCompany.Data
{
    public interface ISearchKeywordsRepository : IRepository<SearchKeywordsDto>
    {
        string GetUrl();

        void Dispose();
    }
}

