﻿namespace WebCompany.Data
{
    public interface IMenuRepository : IRepository<MenuDto>
    {
        string GetUrl();

        void Dispose();
    }
}

