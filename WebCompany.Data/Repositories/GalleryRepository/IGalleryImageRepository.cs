﻿namespace WebCompany.Data
{
    public interface IGalleryImageRepository : IRepository<GalleryImageDto>
    {
        string GetUrl();

        void Dispose();
    }
}

