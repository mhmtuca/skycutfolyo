﻿namespace WebCompany.Data
{
    public interface IGalleryRepository : IRepository<GalleryDto>
    {
        string GetUrl();

        void Dispose();
    }
}

