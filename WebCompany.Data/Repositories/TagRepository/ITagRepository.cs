﻿namespace WebCompany.Data
{
    public interface ITagRepository : IRepository<TagsDto>
    {
        string GetUrl();

        void Dispose();
    }
}

