﻿namespace WebCompany.Data
{
    public interface ICategoryRepository : IRepository<CategoryDto>
    {
        string GetUrl();

        void Dispose();
    }
}

