﻿namespace WebCompany.Data
{
    public class CategoryRepository : Repository<CategoryDto>, ICategoryRepository
    {
        public CategoryRepository(Db context) : base(context) { }

        public string GetUrl()
        {
            return "";
        }
    }
}
