﻿namespace WebCompany.Data
{
    public interface ILanguageRepository : IRepository<LanguageDto>
    {
        string GetUrl();

        void Dispose();
    }
}

