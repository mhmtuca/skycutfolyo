﻿namespace WebCompany.Data
{
    public interface IDocumentRepository : IRepository<DocumentDto>
    {
        string GetUrl();

        void Dispose();
    }
}

