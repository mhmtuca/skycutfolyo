﻿namespace WebCompany.Data
{
    public interface IUrlRepository : IRepository<UrlsDto>
    {
        string GetUrl();

        void Dispose();
    }
}

