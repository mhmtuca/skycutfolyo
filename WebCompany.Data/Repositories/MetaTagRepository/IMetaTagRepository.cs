﻿namespace WebCompany.Data
{
    public interface IMetaTagRepository : IRepository<MetaTagDto>
    {
        string GetUrl();

        void Dispose();
    }
}

