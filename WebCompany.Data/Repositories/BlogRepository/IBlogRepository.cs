﻿namespace WebCompany.Data
{
    public interface IBlogRepository : IRepository<BlogDto>
    {
        string GetUrl();

        void Dispose();
    }
}

