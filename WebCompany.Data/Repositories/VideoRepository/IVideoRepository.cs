﻿namespace WebCompany.Data
{
    public interface IVideoRepository : IRepository<VideoDto>
    {
        string GetUrl();

        void Dispose();
    }
}

