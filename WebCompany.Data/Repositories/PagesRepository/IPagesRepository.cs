﻿namespace WebCompany.Data
{
    public interface IPagesRepository : IRepository<PagesDto>
    {
        string GetUrl();

        void Dispose();
    }
}

