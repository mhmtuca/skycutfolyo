﻿using System;

namespace WebCompany.Data
{
    public class PageContentsRepository : Repository<PageContentDto>, IPageContentsRepository
    {
        public PageContentsRepository(Db context) : base(context) { }

        public string GetUrl()
        {
            return "";
        }
    }
}
