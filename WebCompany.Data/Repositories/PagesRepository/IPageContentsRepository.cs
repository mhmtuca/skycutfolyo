﻿namespace WebCompany.Data
{
    public interface IPageContentsRepository : IRepository<PageContentDto>
    {
        string GetUrl();

        void Dispose();
    }
}

