﻿namespace WebCompany.Data
{
    public class ProductRepository : Repository<ProductsDto>, IProductRepository
    {
        public ProductRepository(Db context) : base(context) { }

        public string GetUrl()
        {
            return "";
        }
    }
}
