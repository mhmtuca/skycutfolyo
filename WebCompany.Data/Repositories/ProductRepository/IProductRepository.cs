﻿using System;
using System.Linq;

namespace WebCompany.Data
{
    public interface IProductRepository : IRepository<ProductsDto>
    {
        string GetUrl();

        void Dispose();
    }
}

