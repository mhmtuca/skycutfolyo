﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace WebCompany.Data
{
    public class PagesDto : BaseMainDto
    {
        [Display(Name = "Başlık")]
        [Required(ErrorMessage ="Başlık alanı boş bırakılamaz")]
        public string Title { get; set; } 
        [Display(Name = "Kısa açıklama")] 
        public string ShortDetail { get; set; }
        [Display(Name = "Uzun açıklama")] 
        public string Detail { get; set; }
        [Display(Name ="Sayaç")]
        [NotMapped]
        public HttpPostedFileBase FileUpload { get; set; }
        public int ViewCount { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Image { get; set; } 
        [Display(Name ="Anasayfa da Göster")]
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
    }
}
