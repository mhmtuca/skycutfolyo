﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCompany.Data
{
    [Table("Message")]
    public class MessageDto :BaseDto
    {
        [Display(Name="Ad soyad")]
        public string FullName { get; set; }
        [Display(Name ="Telefon Numarası")]
        public string PhoneNumber { get; set; }
        [Display(Name ="E-posta")]
        public string Email { get; set; }
        [Display(Name ="Mesaj")]
        public string Message { get; set; }
    }
}
