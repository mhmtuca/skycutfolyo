﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace WebCompany.Data
{
    public class PageContentDto:BaseDto
    {
       

        public Guid ContentId { get; set; }
        [Display(Name = "Başlık")]
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string ContentTitle { get; set; }
        [NotMapped]
        public HttpPostedFileBase FileUpload { get; set; }
        [NotMapped]
        public int PageContentId { get; set; }
        public string ContentText { get; set; }
        public string ContentDetail { get; set; }
        public int ContentType { get; set; } 
    }
}
