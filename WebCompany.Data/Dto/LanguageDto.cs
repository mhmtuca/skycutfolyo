﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Data
{
    public class LanguageDto
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Symbol { get; set; }
        public string Abbreviation { get; set; }
        public bool Deleted { get; set; }
        public bool Actived { get; set; }

        public List<CategoryDto> LanguageCategory { get; set; }
    }
}
