﻿namespace WebCompany.Data
{
    public class GalleryImageDto : BaseDto
    {
      
        public string Title { get; set; } 
        public string Path { get; set; } 
        public int Rows { get; set; }   
        public int GalleryId { get; set; }
        public virtual GalleryDto GalleryList { get; set; }
    }
}
