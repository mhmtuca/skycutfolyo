﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCompany.Data
{
    [Table("MetaTag")]
    public class MetaTagDto :BaseDto
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public int Type { get; set; }
        public Guid ContentId { get; set; } 

    }
}
