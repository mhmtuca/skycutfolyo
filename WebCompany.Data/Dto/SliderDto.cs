﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Data
{
    public class SliderDto : BaseDto
    {
        [Display(Name = "Başlık")] 
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string Name { get; set; }
        [Display(Name = "Fotoğraf")] 
        public string Image { get; set; }
        [Display(Name = "Alt açıklama")]
        public string Title { get; set; }
        [Display(Name = "Detay")]
        public string ShortDetail { get; set; }
        public string Url { get; set; }
        [Display(Name = "Sıra")] 
        public int Rows { get; set; }
        public Guid ContentId { get; set; }
    }
}
