﻿using System;

namespace WebCompany.Data
{
    public class VideoDto :BaseMainDto
    {
        public string Title { get; set; }
        public string VideoCode { get; set; }
        public string Detials { get; set; }
        public string Image { get; set; }
        public int ViewCount { get; set; }
        public int Rows { get; set; }
        public Guid ContentId { get; set; } 
    }
}
