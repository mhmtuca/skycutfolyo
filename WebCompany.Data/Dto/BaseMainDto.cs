﻿namespace WebCompany.Data
{
    public class BaseMainDto :BaseDto
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string Url { get; set; }
    }
}
