﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Data
{
    public class GalleryDto: BaseMainDto
    {
        [Display(Name ="Başlık")]
        [Required(ErrorMessage ="Başlık alanı boş bırakılamaz!")]
        public string Title { get; set; }
        [Display(Name ="Fotoğraf")]
        public string Path { get; set; }
        [Display(Name = "Sırası")]
        public int Rows { get; set; }
        [Display(Name = "Anasayfada göster")]
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
        public virtual List<GalleryImageDto> GalleryImageList { get; set; }
    }
}
