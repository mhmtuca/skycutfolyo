﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Data
{
    public class BaseDto
    {
        public int Id { get; set; }
        [Display(Name = "Aktif/Pasif")]
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
        public Guid CompanyId { get; set; }
        public int LanguageId { get; set; }

        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
