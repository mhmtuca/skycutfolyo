﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Data
{
    public class BlogDto: BaseMainDto
    {
        [Display(Name = "Başlık")]
        [Required(ErrorMessage =  "Başlık alanı boş bırakılamaz")]
        public string Title { get; set; }
        [Display(Name = "Kısa açıklama")]
        public string ShortDetail { get; set; }
        [Display(Name = "Açıklama")]
        public string Detail { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Path { get; set; } 
        public Guid ContentId { get; set; }
        [Display(Name = "Sıra")]
        public int Rows { get; set; }
        [Display(Name = "Anasayfa da göster")]
        public bool IsHomePage { get; set; }
        public int PageView { get; set; }
    }
}
