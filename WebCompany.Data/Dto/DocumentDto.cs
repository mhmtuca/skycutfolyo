﻿using System;

namespace WebCompany.Data
{
    public class DocumentDto:BaseDto
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public int Rows { get; set; }
        public Guid ContentId { get; set; }
    }
}
