﻿namespace WebCompany.Data
{
    public class TagsDto:BaseDto
    { 
        public string Keywords { get; set; }
        public int ContentId { get; set; }  
    }
}
