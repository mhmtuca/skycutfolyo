﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Data
{
    public class MenuDto  
    {
        [Display(Name = "Başlık")] 
        public string Name { get; set; } 
        public int MenuTop { get; set; } 
        [Display(Name="Sıra")]
        public int Rows { get; set; }  
        public int UrlId { get; set; }
        public int Id { get; set; }
        [Display(Name = "Aktif/Pasif")]
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
        public Guid CompanyId { get; set; } 
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public virtual UrlsDto MenuUrl { get; set; }
    }
}
