﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Data
{
    public class SearchKeywordsDto
    {
        public int Id { get; set; }
        [Display(Name="Ara")]
        public string Search { get; set; }
        public Guid ContentId { get; set; }
        public Guid CompanyId { get; set; }
        public int ContentTypeId { get; set; } 
        [Display(Name ="Sil")]
        public bool Deleted { get; set; }
    }
}
