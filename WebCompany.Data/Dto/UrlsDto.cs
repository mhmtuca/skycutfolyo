﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Data
{ 
     public class UrlsDto : BaseDto
    {
        [Display(Name = "Url")]
        [Required(ErrorMessage = "Url alanı boş bırakılamaz")]
        public string Url { get; set; }
        public bool Type { get; set; } 
        public string Title { get; set; }
        public Guid ContentId { get; set; }

        private List<MenuDto> urlmenu;

        public List<MenuDto> UrlMenu
        {
            get
            {
                if (urlmenu == null)
                    urlmenu = new List<MenuDto>();
                return urlmenu;
            }
            set
            {
                urlmenu = value;
            }
        }
    }
}
