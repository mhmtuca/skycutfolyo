﻿using System;
using System.Collections.Generic;
using WebCompany.Business.Models;

namespace WebCompany.Business.Logic
{
    public interface IDocumentLogic
    {
        DocumentListViewModel GetDocumentList();
        DocumentViewModel GetDocumentListByContentId(Guid contentId);
    }
}
