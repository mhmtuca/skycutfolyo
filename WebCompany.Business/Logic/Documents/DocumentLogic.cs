﻿using System;
using System.Linq;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Services;

namespace WebCompany.Business.Logic
{
    public class DocumentLogic : IDocumentLogic
    {
        private readonly IDocumentService _documentService;
        public DocumentLogic(IDocumentService documentService)
        {
            this._documentService = documentService;
        }
        public  DocumentListViewModel GetDocumentList()
        {
            var model = new DocumentListViewModel();
            model.DocumentList = _documentService.GetDocumentList().Select(c=> c.ToModel()).ToList();
            return model;
        }

        public DocumentViewModel GetDocumentListByContentId(Guid contentId)
        {
            throw new NotImplementedException();
        }
    }
}
