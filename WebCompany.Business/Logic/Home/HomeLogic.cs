﻿using System;
using System.Linq;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Services;

namespace WebCompany.Business.Logic
{
    public class HomeLogic : IHomeLogic
    {
        private readonly IProductService productService;
        private readonly ISettingService settingService;
        private readonly IGalleryService galleryService;
        private readonly IBlogService blogService;
        private readonly IPageService pageService;
        public HomeLogic(
        IProductService productService,
        ISettingService settingService,
        IGalleryService galleryService,
        IBlogService blogService,
        IPageService pageService)
        {
            this.productService = productService;
            this.settingService = settingService;
            this.galleryService = galleryService;
            this.blogService = blogService;
            this.pageService = pageService;
        }
        public HomePageViewModel GetHomePage()
        {
            var model = new HomePageViewModel();
            var productList = productService.GetProductList()?.Where(x => x.IsHomePage)?.ToList();
            model.ProductList = productList.Select(f=> f.ToViewModel()).ToList();
            model.PageList = pageService.GetPageList().Where(x => x.IsHomePage).Select(f=> f.ToModel()).ToList();
            var seoTag = settingService.GetSettingListId();
            // var meta=metaTagService?.GetMetaTagContentId(seoTag.ContentId);
            model.MetaTagViewModel=new MetaTagViewModel()
            {
                MetaDescription = seoTag?.MetaDescription,
                MetaKeywords = seoTag?.MetaKeywords,
                MetaTitle = seoTag?.MetaTitle,
                Canonical = WebCompany.Core.UrlExtensions.SiteUrl,
                CreatedDateTime = seoTag.CreatedDateTime,
                UpdatedDateTime = seoTag.UpdateDateTime,
                Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{settingService?.GetLastFirmId()?.Logo}"
            };


            model.BlogViewList = blogService.BlogList().Where(x => x.IsHomePage).OrderBy(x => x.Rows).Select(t=> t.ToModel()).ToList();

            model.GalleryList = galleryService.GalleryList().Where(x => x.IsHomePage).OrderBy(x => x.Rows).Select(f=> f.ToModel()).ToList();

            return model;
        }


    }
}
