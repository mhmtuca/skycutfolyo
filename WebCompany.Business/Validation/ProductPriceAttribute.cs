﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebCompany.Business.Models;

namespace WebCompany.UI.Areas.Admin
{
    public class ProductPriceAttribute : ValidationAttribute
    {
        public ProductPriceAttribute(params string[] propertyNames)
        {
            this.PropertyNames = propertyNames; 
        }
        public string[] PropertyNames { get; private set; } 
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ProductAdminViewModel productModel = (ProductAdminViewModel)validationContext.ObjectInstance;
            if (Convert.ToDecimal(value) < productModel.SalesPrice)
            {
                var errorMessage = FormatErrorMessage((validationContext.DisplayName));
                return new ValidationResult(errorMessage);
            }
            else
            {
                return ValidationResult.Success;
            }

        }
    }
}