﻿using AutoMapper;
using WebCompany.Business.Models;
using WebCompany.Data;

namespace WebCompany.Business.AutoMappers
{
    public static class MapExtensions
    {
        public static MenuViewModel ToModel(this MenuDto entity)
        {
            return Mapper.Map<MenuDto, MenuViewModel>(entity);
        }

        public static MenuDto ToEntity(this MenuViewModel model)
        {
            return Mapper.Map<MenuViewModel, MenuDto>(model);
        }

        #region Product
        public static ProductAdminViewModel ToModel(this ProductsDto entity)
        {
            return Mapper.Map<ProductsDto, ProductAdminViewModel>(entity);
        }

        public static ProductsDto ToEntity(this ProductAdminViewModel model)
        {
            return Mapper.Map<ProductAdminViewModel, ProductsDto>(model);
        }

        public static ProductUIViewModel ToViewModel(this ProductsDto entity)
        {
            return Mapper.Map<ProductsDto, ProductUIViewModel>(entity);
        }

        public static ProductsDto ToEntity(this ProductUIViewModel model)
        {
            return Mapper.Map<ProductUIViewModel, ProductsDto>(model);
        }
        #endregion


        #region Gallery
        public static GalleryImageModel ToModel(this GalleryImageDto entity)
        {
            return Mapper.Map<GalleryImageDto, GalleryImageModel>(entity);
        }

        public static GalleryImageDto ToEntity(this GalleryImageModel model)
        {
            return Mapper.Map<GalleryImageModel, GalleryImageDto>(model);
        }


        public static GalleryViewModel ToModel(this GalleryDto entity)
        {
            return Mapper.Map<GalleryDto, GalleryViewModel>(entity);
        }

        public static GalleryDto ToEntity(this GalleryViewModel model)
        {
            return Mapper.Map<GalleryViewModel, GalleryDto>(model);
        }

        #endregion
        #region Blog
        public static BlogViewModel ToModel(this BlogDto entity)
        {
            return Mapper.Map<BlogDto, BlogViewModel>(entity);
        }

        public static BlogDto ToEntity(this BlogViewModel model)
        {
            return Mapper.Map<BlogViewModel, BlogDto>(model);
        }


        #endregion
        #region Page
        public static PageViewModel ToModel(this PagesDto entity)
        {
            return Mapper.Map<PagesDto, PageViewModel>(entity);
        }

        public static PagesDto ToEntity(this PageViewModel model)
        {
            return Mapper.Map<PageViewModel, PagesDto>(model);
        }


        #endregion
        #region Category
        public static CategoryAdminViewModel ToModel(this CategoryDto entity)
        {
            return Mapper.Map<CategoryDto, CategoryAdminViewModel>(entity);
        }

        public static CategoryDto ToEntity(this CategoryAdminViewModel model)
        {
            return Mapper.Map<CategoryAdminViewModel, CategoryDto>(model);
        }
        public static CategoryViewModel ToViewModel(this CategoryDto entity)
        {
            return Mapper.Map<CategoryDto, CategoryViewModel>(entity);
        }

        public static CategoryDto ToEntity(this CategoryViewModel model)
        {
            return Mapper.Map<CategoryViewModel, CategoryDto>(model);
        }
        #endregion

        #region MetaTag
        public static MetaTagAdminViewModel ToModel(this MetaTagDto entity)
        {
            return Mapper.Map<MetaTagDto, MetaTagAdminViewModel>(entity);
        }

        public static MetaTagDto ToEntity(this MetaTagAdminViewModel model)
        {
            return Mapper.Map<MetaTagAdminViewModel, MetaTagDto>(model);
        }
        #endregion

        #region Video
        public static VideoModel ToModel(this VideoDto entity)
        {
            return Mapper.Map<VideoDto, VideoModel>(entity);
        }

        public static VideoDto ToEntity(this VideoModel model)
        {
            return Mapper.Map<VideoModel, VideoDto>(model);
        }


        #endregion
        #region Document
        public static DocumentViewModel ToModel(this DocumentDto entity)
        {
            return Mapper.Map<DocumentDto, DocumentViewModel>(entity);
        }

        public static DocumentDto ToEntity(this DocumentViewModel model)
        {
            return Mapper.Map<DocumentViewModel, DocumentDto>(model);
        }


        #endregion
        #region Language
        public static LanguageModel ToModel(this LanguageDto entity)
        {
            return Mapper.Map<LanguageDto, LanguageModel>(entity);
        }

        public static LanguageDto ToEntity(this LanguageModel model)
        {
            return Mapper.Map<LanguageModel, LanguageDto>(model);
        }


        #endregion
        
        #region AddSite
        public static AddSiteModel ToModel(this SettingDto entity)
        {
            return Mapper.Map<SettingDto, AddSiteModel>(entity);
        }

        public static SettingDto ToEntity(this AddSiteModel model)
        {
            return Mapper.Map<AddSiteModel, SettingDto>(model);
        }


        #endregion
    }
}