﻿using AutoMapper;
using WebCompany.Business.Models;
using WebCompany.Core.Extensions;
using WebCompany.Data;

namespace WebCompany.Business.AutoMappers
{
    public static class AutoMapperSite
    {
        public static void Run()
        {
            Configure();
        }
        public static void Configure()
        {
            Mapper.Initialize(
                config =>
                {
                    config.CreateMap<MenuDto, MenuViewModel>();
                    config.CreateMap<MenuViewModel, MenuDto>();

                    config.CreateMap<ProductsDto, ProductAdminViewModel>()
                    .ForMember(d => d.PriceText, opt => opt.MapFrom(f => f.Price.FormatDecimal()))
                    .ForMember(d => d.SalesPriceText, opt => opt.MapFrom(f => f.SalesPrice.FormatDecimal()))
                    .ForMember(d => d.Price, opt => opt.MapFrom(f => f.Price.ConvertDecimal()))
                    .ForMember(d => d.SalesPrice, opt => opt.MapFrom(f => f.SalesPrice.ConvertDecimal()))
                    .ForMember(d => d.ContentId, opt => opt.MapFrom(f => f.ContentId))
                    .ForMember(d => d.Id, opt => opt.MapFrom(f => f.Id));

                    config.CreateMap<ProductAdminViewModel, ProductsDto>();
           


                    config.CreateMap<CategoryDto, CategoryAdminViewModel>();
                    config.CreateMap<CategoryAdminViewModel, CategoryDto>();

                    config.CreateMap<CategoryDto, CategoryIndexViewModel>();
                    config.CreateMap<CategoryIndexViewModel, CategoryDto>();

                    config.CreateMap<MetaTagDto, MetaTagAdminViewModel>();
                    config.CreateMap<MetaTagAdminViewModel, MetaTagDto>();

                    config.CreateMap<GalleryImageDto, GalleryImageModel>();
                    config.CreateMap<GalleryImageModel, GalleryImageDto>();

                    config.CreateMap<VideoDto, VideoModel>();
                    config.CreateMap<VideoModel, VideoDto>();

                    config.CreateMap<LanguageDto, LanguageModel>();
                    config.CreateMap<LanguageModel, LanguageDto>();

                    config.CreateMap<SettingDto, AddSiteModel>();
                    config.CreateMap<AddSiteModel, SettingDto>();

                    config.CreateMap<DocumentDto, DocumentViewModel>();
                    config.CreateMap<DocumentViewModel, DocumentDto>();

                    config.CreateMap<ProductsDto, ProductViewModel>();
                    config.CreateMap<ProductViewModel, ProductsDto>();

                    config.CreateMap<PagesDto, PageViewModel>();
                    config.CreateMap<PageViewModel, PagesDto>();

                    config.CreateMap<BlogDto, BlogViewModel>();
                    config.CreateMap<BlogViewModel, BlogDto>();

                    config.CreateMap<GalleryDto, GalleryViewModel>();
                    config.CreateMap<GalleryViewModel, GalleryDto>();
                });
        }
    
    }
}