﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Business.Models
{
    public class BlogViewModel:BaseViewModel
    {
        [Display(Name = "Başlık")] 
        public string Title { get; set; }
        [Display(Name = "Kısa açıklama")]
        public string ShortDetail { get; set; }
        [Display(Name = "Açıklama")]
        public string Detail { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Path { get; set; }
        public Guid ContentId { get; set; }
        [Display(Name = "Sıra")]
        public int Rows { get; set; }
        [Display(Name = "Anasayfa da göster")]
        public bool IsHomePage { get; set; }
        public int PageView { get; set; }
    }
}
