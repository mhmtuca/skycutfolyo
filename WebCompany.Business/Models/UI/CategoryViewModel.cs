﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCompany.UI.Areas.Admin;

namespace WebCompany.Business.Models
{
    public class CategoryViewModel:BaseViewModel
    {
       
        public string Name { get; set; }
        public string Url { get; set; } 
        public string Detail { get; set; }
        public int LanguageId { get; set; }
        public Guid ContentId { get; set; }
        private List<ProductViewModel> categoryProducts;

        public List<ProductViewModel> CategoryProducts
        {
            get
            {
                if (categoryProducts == null)
                    categoryProducts = new List<ProductViewModel>();
                return categoryProducts;
            }
            set
            {
                categoryProducts = value;
            }
        }
    }
}