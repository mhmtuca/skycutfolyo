﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class CategoryIndexViewModel  : MetaTagViewModel
    {
        private List<CategoryViewModel> categoryListModel;

        public List<CategoryViewModel> CategoryListModel
        {
            get
            {
                return categoryListModel ?? new List<CategoryViewModel>();
            }
            set { categoryListModel = value; }
        }

    }
}