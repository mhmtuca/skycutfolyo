﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class DocumentViewModel : BaseViewModel
    {
        public string Title { get; set; }
        public string Path { get; set; }
        public int Rows { get; set; }
        public Guid ContentId { get; set; }
    }
}