﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCompany.Data;

namespace WebCompany.UI.Models
{
    public class PageContentModel
    {
        public Guid ContentId { get; set; }
        public string ContentTitle { get; set; }
        public string ContentText { get; set; }
        public string ContentDetail { get; set; }
        public int ContentType { get; set; } 
        public int Id { get; set; } 
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }

}