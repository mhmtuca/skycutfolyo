﻿using System;
using System.Web;

namespace WebCompany.Business.Models
{
    public class GalleryImageModel
    { 
        public int Id { get; set; }
        public string Title { get; set; }
        public int GalleryId { get; set; }
        public string Path { get; set; }
        public HttpPostedFileBase File { get; set; }
        public int Rows { get; set; }
        public int CompanyId { get; set; }
        public bool Deleted { get; set; }
        public bool Actived { get; set; }  
        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}