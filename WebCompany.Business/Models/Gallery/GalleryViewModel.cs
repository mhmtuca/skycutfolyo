﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Business.Models 
{
    public class GalleryViewModel:BaseViewModel
    {
        public string Path { get; set; }
        public string Title { get; set; }
        public int Rows { get; set; }
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
    }
}
