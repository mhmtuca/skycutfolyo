﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class GalleryModel
    {
         
        public string Path { get; set; }
        public int Rows { get; set; }
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
    }
}