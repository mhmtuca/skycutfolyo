﻿using System.Collections.Generic;

namespace WebCompany.Business.Models
{
    public class GalleryImageViewModel
    {
       public GalleryImageModel GalleryImageModel { get; set; }
       public string Title { get; set; }
       public string Path { get; set; }

        public int GalleryId { get; set; }

        public List<GalleryImageModel> GalleryImageListModel { get; set; }
    }
}