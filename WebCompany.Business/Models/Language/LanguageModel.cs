﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class LanguageModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Symbol { get; set; }
        public string Abbreviation { get; set; }
        public bool Deleted { get; set; }
        public bool Actived { get; set; }
    }
}