﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebCompany.Data;

namespace WebCompany.Business.Models
{
    public class CategoryModel : BaseViewModel
    {
        [Display(Name = "Başlık")]
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string Name { get; set; }
        [Display(Name = "Açıklama")]
        public string Detail { get; set; }

        public string MetaTitle { get; set; }

        public string MetaDescription { get; set; }

        public string MetaKeywords { get; set; }
        public Guid ContentId { get; set; }
        private List<ProductsDto> categoryProducts;
        public virtual LanguageDto CategoryLanguage { get; set; }
        public List<ProductsDto> CategoryProducts
        {
            get
            {
                if (categoryProducts == null)
                    categoryProducts = new List<ProductsDto>();
                return categoryProducts;
            }
            set
            {
                categoryProducts = value;
            }
        }

    }
}