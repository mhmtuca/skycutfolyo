﻿using System.Collections.Generic;
using WebCompany.Business.Models;
using WebCompany.Data;

namespace WebCompany.UI.Models
{
    public class SiteModel : BaseSiteModel
    {

        public SiteModel()
        {
            this.ProductListModel = new List<ProductsDto>();
            this.ProductModel = new ProductsDto();
            this.MetaTagListModel = new List<MetaTagDto>();
            this.MetaTagModel = new MetaTagDto();
        }
        public CategoryDto CategoryModel { get; set; }
        public List<CategoryDto> CategoryListModel { get; set; }

        public List<LanguageDto> LanguageListModel { get; set; }
        public TagsDto TagViewModel { get; set; }
        public List<TagsDto> TagViewListModel { get; set; }
        public SettingDto SettingModel { get; set; }
        public List<SettingDto> SettingListModel { get; set; }
        public MetaTagDto MetaTagModel { get; set; }
        public List<MetaTagDto> MetaTagListModel { get; set; }
        public ProductsDto ProductModel { get; set; }
        public List<ProductsDto> ProductListModel { get; set; }
        public PagesDto PageModel { get; set; }
        public List<PagesDto> PageListModel { get; set; }
        public SearchKeywordsDto SearchKeywordsModel { get; set; }
        public List<SearchKeywordsDto> SearchKeywordsListModel { get; set; }
        public SliderDto SliderModel { get; set; }
        public List<SliderDto> SliderListModel { get; set; }
        public MenuDto MenuModel { get; set; }

        private List<MenuDto> menuListModel;
        private MetaTagViewModel metaTagViewModel;

        public MetaTagViewModel GetMetaTagViewModel()
        {
            return metaTagViewModel;
        }

        public void SetMetaTagViewModel(MetaTagViewModel value)
        {
            metaTagViewModel = value;
        }

        public MessageDto MessageModel { get; set; }

        public List<MessageDto> MessageModelList { get; set; }

        public List<MenuDto> MenuListModel
        {
            get
            {
                if (menuListModel == null)
                    menuListModel = new List<MenuDto>();
                return menuListModel;
            }
            set { menuListModel = value; }
        } 
        public SeoTag SeoTag { get; set; }

        public GalleryDto GalleryModel { get; set; }
        public List<GalleryDto> GalleryModelList { get; set; }

        public GalleryImageDto GalleryImageModel { get; set; }
        public List<GalleryImageDto> GalleryImageModelList { get; set; }

        public BlogDto BlogModel { get; set; }
        public List<BlogDto> BlogModelList { get; set; }

        public DocumentDto DocumentModel { get; set; }
        public List<DocumentDto> DocumentModelList { get; set; }

        public VideoDto VideoModel { get; set; }
        public List<VideoDto> VideoModelList { get; set; }


        public UrlsDto UrlsModel { get; set; }
        public List<UrlsDto> UrlsModelList { get; set; }

        public PageContentDto PageContent{ get; set; }
        public List<PageContentDto> PageContentList { get; set; }
    }
    public class SeoTag
    {
        public string MetaTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
    }
}