﻿namespace WebCompany.UI.Models
{
    public class BaseSiteModel
    {
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int Page { get; set; }
        public int Pages { get; set; }
    }
}