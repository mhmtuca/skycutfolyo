﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebCompany.Data;

namespace WebCompany.Business.Models
{
    public class MenuCreateViewModel 
    {
        public MenuCreateViewModel()
        {
            this.MenuList = new List<MenuViewModel>();
            this.Urls = new List<UrlsDto>();
            this.Menu = new MenuViewModel();
        }
        public List<MenuViewModel> MenuList { get; set; }
        public MenuViewModel Menu { get; set; } 
        public List<UrlsDto> Urls { get; set; }
        //public List<CategoryDto> CategoriesModel { get; set; }
    }

    public class MenuViewModel  
    {
        public int Id { get; set; }
       
        [Display(Name = "Menü Adı")]
        public string Name { get; set; } 
        public int MenuTop { get; set; }
        [Display(Name = "Type")]
        public int UrlId { get; set; } 
        public Guid CompanyId { get; set; }
        [Display(Name = "Sırası")]
        [Required(ErrorMessage = "Lütfen bir değer giriniz!")]
        [Range(0, 1000, ErrorMessage = "Lütfen 0 ile 1000 arası bir sıra giriniz")]
        public int Rows { get; set; }
        [Display(Name = "Aktif")]
        public bool Actived { get; set; }
        public bool Deleted { get; set; }  
    }
}