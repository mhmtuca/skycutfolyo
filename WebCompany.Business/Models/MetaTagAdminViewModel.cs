﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class MetaTagAdminViewModel
    {
        [Display(Name = "Başlık")]
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string MetaTitle { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description alanı boş bırakılamaz")]
        public string MetaDescription { get; set; }
        [Display(Name = "Seo Keywords")]
        [Required(ErrorMessage = "Seo Keywords alanı boş bırakılamaz")]
        public string MetaKeywords { get; set; }
        public int Type { get; set; }
        public Guid ContentId { get; set; }
    }
}