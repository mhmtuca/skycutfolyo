﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class BaseViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Aktif/Pasif")]
        public bool Actived { get; set; }
        public bool Deleted { get; set; }
        public Guid CompanyId { get; set; }
        public int LanguageId { get; set; }
        [Display(Name = "Meta Title")]
        [Required(ErrorMessage = "Meta title alanı boş bırakılamaz!")]
        public string MetaTitle { get; set; }
        [Display(Name = "Meta Description")]
        [Required(ErrorMessage = "Meta description alanı boş bırakılamaz!")]
        public string MetaDescription { get; set; }
        [Display(Name = "Meta Keywords")]
        [Required(ErrorMessage = "Meta keywords alanı boş bırakılamaz!")]
        public string MetaKeywords { get; set; }

        public DateTime CreatedDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}