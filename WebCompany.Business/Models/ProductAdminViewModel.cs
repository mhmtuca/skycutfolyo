﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebCompany.Data;
using WebCompany.UI.Areas.Admin;

namespace WebCompany.Business.Models
{
    public class ProductAdminViewModel :BaseViewModel
    {
        [Display(Name = "Ürün Adı")]
        [Required(ErrorMessage = "Ürün Adı alanı boş bırakılamaz")]
        public string Name { get; set; }
        public string Url { get; set; }
        [Display(Name = "Kısa açıklama")]
        public string ShortDetail { get; set; }
        [Display(Name = "Açıklama")]
        public string Detail { get; set; }


        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Display(Name = "Satış fiyatı")]
        [Range(1, 99999999, ErrorMessage = "Fiyat için 1 ile 99999999 değeri girmelisiniz")]
        [Required(ErrorMessage = "Satış fiyat alanı boş bırakılamaz")]
        public decimal SalesPrice { get; set; }
        public int LanguageId { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Display(Name = "Fiyatı")]
        [Range(1, 99999999, ErrorMessage = "Fiyat için 1 ile 99999999 değeri girmelisiniz")]
        [Required(ErrorMessage = "Fiyat alanı boş bırakılamaz")]
        [ProductPriceAttribute(ErrorMessage = " Ürün fiyatı satış fiyatından az olamaz")]
        public decimal Price { get; set; }

        public string PriceText { get; set; }
        public string SalesPriceText { get; set; } 
        public int Rate
        {
            get
            {
                if (Price!=0 &&  SalesPrice!=0)
                {
                    return Convert.ToInt16(Math.Ceiling(((Price - SalesPrice) / Price) * 100));
                }
                return 0;
               
            }
        }
        [Display(Name = "Kategori")]
        public int CategoryId { get; set; }
        [Display(Name = "Para Birimi")]
        public int CurrencyUnit { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Image { get; set; }
        [Display(Name = "Fotoğraf 2")]
        public string Image2 { get; set; }
        [Display(Name = "Fotoğraf 3")]
        public string Image3 { get; set; }
        [Display(Name = "Fotoğraf 4")]
        public string Image4 { get; set; }
        [Display(Name = "Etiket")]
        public string Tag { get; set; }
        [Display(Name = "Anasayfada göster")]
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }


        public CategoryAdminViewModel ProductCategory;

        private List<CategoryAdminViewModel> productCategoryList;

        public List<CategoryAdminViewModel> ProductCategoryList
        {
            get { return productCategoryList ?? new List<CategoryAdminViewModel>(); }
            set { productCategoryList = value; }
        }
       
    }

    public class ProductViewModel
    {
       
        public List<CategoryAdminViewModel> ProductCategoryList { get; set; }
        public List<ProductAdminViewModel> ProductAdminViewModel { get; set; }
        public List<LanguageDto> LanguageList { get; set; }
    }
}