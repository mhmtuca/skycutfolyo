﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class AddSiteModel
    {
        public int Id { get; set; }
      
        [Display(Name = "Başlık")]
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string Title { get; set; }
        [Display(Name = "Logo")]
   
        public string Logo { get; set; }
        [Required(ErrorMessage = "Logo alanı boş bırakılamaz")]
        public HttpPostedFileBase Image { get; set; }
        public string Icon { get; set; }
        [Display(Name = "Anasayfa reklam başlığı")] 
        public string HomeContactTitle { get; set; }
        [Display(Name = "Anasayfa reklam fotoğrafı")]
        public string HomeContactPath { get; set; }
        [Display(Name = "Anasayfa reklam açıklaması")]
        public string HomeContactDetail { get; set; }
        [Display(Name = "Anasayfa reklam telefon numarası")]
        public string HomeContactPhone { get; set; }
        [Display(Name = "Anasayfa reklam url")]
        public string HomeContactUrl { get; set; }
        [Display(Name = "Anasayfa reklam göster")]
        public bool IsHomeContact { get; set; }
        [Display(Name = "Telefon numarası")]
        [Required(ErrorMessage = "Telefon alanı boş bırakılamaz")]
        public string Phone { get; set; }
        [Display(Name = "Email numarası")]
        [Required(ErrorMessage = "Email alanı boş bırakılamaz")]
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Google { get; set; }
        public string Youtube { get; set; }
        public string Instagram { get; set; }
        public string Linkedin { get; set; }
        public string CopyRight { get; set; }
        public string GoogleAnaltiycs { get; set; }
        public string GoogleMaps { get; set; }
        public string Url { get; set; }
        [Display(Name = "Anasayfa kısa söz başlık")]
        public string HeadLine { get; set; }
        [Display(Name = "Anasayfa kısa söz açıklama")]
        public string HeadLineText { get; set; }
        [Display(Name = "Anasayfa kısa söz başlık 2")]
        public string HeadLine2 { get; set; }
        [Display(Name = "Anasayfa kısa söz açıklama 2")]
        public string HeadLineText2 { get; set; }
        [Display(Name = "Anasayfa kısa söz başlık 3")]
        public string HeadLine3 { get; set; }
        [Display(Name = "Anasayfa kısa söz açıklama 3")]
        public string HeadLineText3 { get; set; }
        [Display(Name = "Anasayfa kısa söz başlık 4")]
        public string HeadLine4 { get; set; }
        [Display(Name = "Anasayfa kısa söz açıklama 4")]
        public string HeadLineText4 { get; set; }

        [Display(Name = "Anasayfa alt bölüm başlık")]
        public string Footer { get; set; }
        [Display(Name = "Anasayfa alt bölüm açıklama")]
        public string FooterText { get; set; }
        [Display(Name = "Anasayfa alt bölüm başlık 2")]
        public string Footer2 { get; set; }
        [Display(Name = "Anasayfa alt bölüm açıklama 2")]
        public string FooterText2 { get; set; }
        public Guid ContentId { get; set; }
        [Display(Name = "Aktif")]
        public bool Actived { get; set; }
        [Display(Name = "Sil")]
        public bool Deleted { get; set; }
        [Display(Name = "Ekleme Tarihi")]
        public DateTime CreatedDateTime { get; set; }
        [Display(Name = "Değiştirme Tarihi")]
        public DateTime UpdateDateTime { get; set; }
        [Display(Name = "Anasayfa slider göster")]
        public bool IsSliderHomePage { get; set; }
        public int LanguageId { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
    }
}