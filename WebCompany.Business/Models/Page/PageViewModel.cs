﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebCompany.Business.Models
{
    public class PageViewModel:BaseViewModel
    {
        [Display(Name = "Başlık")]
        public string Title { get; set; }
        [Display(Name = "Kısa açıklama")]
        public string ShortDetail { get; set; }
        [Display(Name = "Uzun açıklama")]
        public string Detail { get; set; }
        [Display(Name = "Sayaç")] 
        public int ViewCount { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Image { get; set; }
        [Display(Name = "Anasayfa da Göster")]
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
    }
}
