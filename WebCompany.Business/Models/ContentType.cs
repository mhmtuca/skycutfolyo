﻿namespace WebCompany.UI.Models
{
    public enum ContentType : int
    {
        Category = 1,
        Product =2,
        Page =3,
        CompanySite =4,
        Blog=5
    }
}