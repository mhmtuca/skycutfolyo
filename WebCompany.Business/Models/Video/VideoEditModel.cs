﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class VideoEditModel
    {
        public List<VideoModel> VideoModelList { get; set; }
        public List<LanguageModel> LanguageListModel{ get; set; }
    }
}