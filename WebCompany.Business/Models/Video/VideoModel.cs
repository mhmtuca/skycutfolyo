﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class VideoModel : BaseViewModel
    {
        [Display(Name ="Başlık")]
        [Required(ErrorMessage = "Başlık alanı boş bırakılamaz")]
        public string Title { get; set; }
        [Display(Name = "Video Kodu")]
        [Required(ErrorMessage = "Video kodu alanı boş bırakılamaz!")]
        public string VideoCode { get; set; }
        [Display(Name = "Video Detay")]
        [Required(ErrorMessage = "Video detay alanı boş bırakılamaz!")]
        public string Detials { get; set; }
        [Display(Name = "Fotoğraf")]
        public string Image { get; set; }

        public HttpPostedFileBase Photo { get; set; }
        public int ViewCount { get; set; }
        [Display(Name = "Sıra")]
        public int Rows { get; set; }
        public string Url { get; set; }
        public Guid ContentId { get; set; }
    }
}