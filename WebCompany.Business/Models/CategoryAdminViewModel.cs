﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.Business.Models
{
    public class CategoryAdminViewModel:BaseViewModel
    {
         
        public string Name { get; set; }
        public string Url { get; set; } 
        public string Detail { get; set; }
        public Guid ContentId { get; set; }
    }
}