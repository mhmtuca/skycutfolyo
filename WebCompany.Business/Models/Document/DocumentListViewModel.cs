﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCompany.Business.Models
{
    public class DocumentListViewModel
    {
        public List<DocumentViewModel> DocumentList { get; set; }
        public MetaTagViewModel MetaTagViewModel { get; set; }
    }

  
}
