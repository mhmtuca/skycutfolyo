﻿using System.Collections.Generic;

namespace WebCompany.Business.Models
{
    public class HomePageViewModel
    {
        public List<ProductUIViewModel> ProductList { get; set; }
        public MetaTagViewModel MetaTagViewModel { get; set; }
        public List<BlogViewModel> BlogViewList { get; set; }

        public List<GalleryViewModel> GalleryList { get; set; }

        public List<PageViewModel> PageList { get; set; }
    }
}
