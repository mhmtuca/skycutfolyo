﻿using System;

namespace WebCompany.Business.Models
{
    public class ProductUIViewModel:BaseViewModel
    {
        public string Name { get; set; }
        public string ShortDetail { get; set; }
        public string Detail { get; set; }

        public decimal Price { get; set; }
        public decimal SalesPrice { get; set; }
        public int Rate { get; set; }
        public int CategoryId { get; set; }
        public int CurrencyUnit { get; set; }
        public string Image { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Tag { get; set; }
        public bool IsHomePage { get; set; }
        public Guid ContentId { get; set; }
 
    }
}
