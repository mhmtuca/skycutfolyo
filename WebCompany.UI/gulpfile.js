﻿// including plugins
var gulp = require('gulp');
var minifyCss = require("gulp-minify-css"); 
var uglify = require('gulp-uglify'); 
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var livereload = require("gulp-livereload"); 

// task
gulp.task('style', function () {
    gulp.src('./Content/assets/css/*.css') // path to your files
    .pipe(minifyCss())
    .pipe(rename("min"))
    .pipe(concat('./min/site-min.css'))
    .pipe(gulp.dest('./Content/assets/css/'));
    livereload.reload();
});

gulp.task('scripts', function () {
    return gulp.src([
        './Content/assets/dist/jquery.min.js',
        './Content/assets/dist/bootstrap.bundle.js',
        './Content/assets/js/*.js'
    ])
        .pipe(rename({ dirname: 'min' }))
        .pipe(uglify())
        .pipe(concat('./min/site-min.js'))
        .pipe(gulp.dest('./Content/assets/js/'));
 
});