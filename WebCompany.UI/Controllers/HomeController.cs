﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebCompany.Business.Logic;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMetaTagService metaTagService;
        private readonly IHomeLogic homeLogic;
        private readonly IUrlsService urlsService;
        public HomeController(
        IMetaTagService metaTagService, 
        IUrlsService urlsService,
        IHomeLogic homeLogic)
        {
            this.homeLogic = homeLogic;
            this.metaTagService = metaTagService; 
            this.urlsService = urlsService;
        }
        // GET: Admin/Home
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index()
        {
           return View(homeLogic.GetHomePage());
        }
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult SiteMap()
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            SiteMapFeedGenerator gen = new SiteMapFeedGenerator(Response.Output);

            gen.WriteStartDocument();
            gen.WriteItem(WebCompany.Core.UrlExtensions.SiteUrl, DateTime.Now, "1.0");
            var urls = urlsService.GetUrlList();
            foreach (var item in urls)
            {
                gen.WriteItem(WebCompany.Core.UrlService.WebViewUrl(item.Url), DateTime.Now, "1.0");
            }
             
            gen.WriteEndDocument();
            gen.Close();
            return View();
        }
         
      
    }
}