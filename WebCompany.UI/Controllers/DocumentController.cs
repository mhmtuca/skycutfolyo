﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.Logic;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class DocumentController : Controller
    {
        private IDocumentLogic documentLogic;
        private IPageService pageService; 
        public DocumentController(IDocumentLogic documentLogic, IPageService pageService)
        {
            this.documentLogic = documentLogic;
            this.pageService = pageService; 
        }
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult List(Guid? contentId)
        {
                         
                var model = new DocumentListViewModel();
                model= documentLogic.GetDocumentList();
                  
                return View(model);
            
           

        }

        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(Guid? contentId)
        {
            if (contentId.HasValue)
            {
                var model = new SiteModel();
                var page = pageService.GetPageContentId(contentId.Value);
                model.PageModel = page;
                model.PageListModel = pageService.GetPageList();
                //var meta = metaTagService.GetMetaTagContentId(contentId.Value);

                model.SetMetaTagViewModel(new MetaTagViewModel()
                {
                    Canonical = "",
                    ContentId = page.ContentId,
                    CreatedDateTime = page.CreatedDateTime,
                    Img = $"{Core.UrlExtensions.SiteUrl}/content/img/p/desktop/{page.Image}",
                    MetaDescription = page.MetaDescription,
                    MetaKeywords = page.MetaKeywords,
                    MetaTitle = page.MetaTitle,
                    UpdatedDateTime = page.UpdateDateTime
                });

                return View(model);
            }
            return RedirectPermanent("/");

        }
    }
}