﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class GalleryController : Controller
    {
        #region Members

        private readonly IGalleryService galleryService;
        private readonly IMetaTagService metaTagService;
        private readonly ISettingService settingService;
        #endregion

        #region Ctr

        public GalleryController(
        IGalleryService galleryService,
        IMetaTagService metaTagService,
        ISettingService settingService
        )
        {
            this.galleryService = galleryService;
            this.metaTagService = metaTagService;
            this.settingService = settingService;
        }
        #endregion
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(int page=1)
        {
            
            var galleriList = galleryService.GalleryList();
            var model = new SiteModel();
            int pageSize = WebCompany.Core.UrlExtensions.PageSize;
            int pageCount = galleriList.Count();
            model.PageSize = pageSize;
            model.PageCount = pageCount;
            model.Page = page;
            if (page!=1 && page>Convert.ToInt16(Math.Ceiling(Convert.ToDouble(pageCount / pageSize))))
            {
                throw new HttpException(404, "Not found");
            }

            model.Pages = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(pageCount / pageSize))) == 0 ? 1 : Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize)));
            model.GalleryModelList = galleriList.Skip((page - 1) * pageSize)?.Take(pageSize).ToList();
            model.SetMetaTagViewModel(new MetaTagViewModel()
            {
                MetaTitle = $"Galeri | {Core.UrlExtensions.SiteName } galeri listesi" ,
                MetaDescription = "Galeri sayfamızda ürünlerle ilgili fotoğrafları bulabilirsiniz.",
                MetaKeywords = "blog, ürün, kategori ,site, folyo, plotter, galeri, kesim, baskı",
                Canonical = $"{Core.UrlExtensions.SiteUrl}/galeri/1",
                CreatedDateTime = galleriList.OrderBy(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime,
                UpdatedDateTime = galleriList.OrderByDescending(x => x.UpdateDateTime).FirstOrDefault().UpdateDateTime,
                Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{settingService.GetLastFirmId().Logo}"
            });
            return View(model);
        }
    }
}