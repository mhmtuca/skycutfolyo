﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class BlogController : Controller
    {
        #region Members

        private readonly IBlogService _blogService;
        private readonly IMetaTagService _metaTagService;
        private readonly ISettingService _settingService;
        #endregion
        // GET: Blog 
        #region Ctr
      
        public BlogController(IBlogService blogService, IMetaTagService metaTagService, ISettingService settingService)
        {
            this._blogService = blogService;
            this._metaTagService = metaTagService;
            this._settingService = settingService;
        }
        #endregion

        #region Controller 
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult List(byte page=0)
        {
            var blogList= _blogService.BlogList();
            var model = new SiteModel();
            int pageSize = WebCompany.Core.UrlExtensions.PageSize;
            int pageCount = blogList.Count();
            model.PageSize = pageSize;
            model.PageCount = pageCount;
            model.Page = page;
            //if (page == 0  || (page * pageSize> pageCount))
            //{ 
            //    throw new HttpException(404, "Not found");
            //}
            model.Pages = Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize))) == 0 ? 1 : Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize)));
            model.BlogModelList = blogList;
            model.SetMetaTagViewModel(new MetaTagViewModel() {
               MetaTitle = $"Blog | {Core.UrlExtensions.SiteName } sitesindeki tüm blog başlıklarını bulabilirsiniz | {page}",
               MetaDescription = "Bu sayfada sitemizdeki güncel blogları bulabilirsiniz. Folyo kesim fiyatları ve folyo kesim plotter hakkındaki tüm ürünleri, bilgileri buradan takip edebilirsiniz.",
               MetaKeywords = "blog, ürün, kategori ,site, reklam, skycut, folyo kesim, ümraniye",
               Canonical = $"{Core.UrlExtensions.SiteUrl}/blogs/1",
               CreatedDateTime = blogList.Any() ? blogList.OrderBy(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime : DateTime.Now,
               UpdatedDateTime = blogList.Any() ? blogList.OrderByDescending(x => x.UpdateDateTime).FirstOrDefault().UpdateDateTime : DateTime.Now,
               Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{_settingService.GetLastFirmId().Logo}",
               ContentId = blogList.Any() ? blogList.FirstOrDefault().ContentId :Guid.NewGuid()
            });
            return View(model);
        }
       
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(Guid? contentId)
        {

            var model = new SiteModel();
            if (contentId.HasValue)
            {
                model.BlogModelList = _blogService.BlogList();
                var blog= _blogService.GetBlogContentId(contentId.Value);
                model.BlogModel = blog;
                // var metaContent= _metaTagService.GetMetaTagContentId(blog.ContentId); 
                model.SetMetaTagViewModel(new MetaTagViewModel()
                {
                    MetaTitle = blog.MetaTitle,
                    MetaDescription = blog.MetaDescription,
                    MetaKeywords = blog.MetaKeywords,
                    Canonical = "",// WebCompany.Core.UrlService.WebViewUrl(blog.Url),
                    CreatedDateTime = blog.CreatedDateTime,
                    UpdatedDateTime = blog.UpdateDateTime,
                    Img = WebCompany.Core.UrlService.ImagePathHomeView(WebCompany.Core.UrlExtensions.BlogViewPath, "desktop", blog.Path)
                });
            }
            return View(model);
        }
        #endregion
    }
}