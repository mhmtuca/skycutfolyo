﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class CategoryController : Controller
    {
        private IProductService productService;
        private IPageService pageService;
        private ICategoryService categoryService;
        private ISettingService settingService;
        private IMetaTagService metaTagService;
        public CategoryController(IProductService productService,
       IPageService pageService,
       ICategoryService categoryService,
       ISettingService settingService,
       IMetaTagService metaTagService)
        {
            this.productService = productService;
            this.pageService = pageService;
            this.categoryService = categoryService;
            this.settingService = settingService;
            this.metaTagService = metaTagService;
        }
        // GET: Admin/Home
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index()
        {
            var model = new CategoryIndexViewModel();
            model.CategoryListModel = categoryService.GetCategoryList().Select(x => x.ToViewModel()).ToList();
            var categoryList = pageService.GetPageList().Where(x => x.IsHomePage).ToList(); 

            model.MetaTitle = $" Kategori  | {Core.UrlExtensions.SiteName } | sitemizdeki tüm ürün kategorileri ";
            model.MetaDescription = "Sitedeki tüm ürün kategorileri buradan takip edebilirsiniz.";
            model.MetaKeywords = "ürün, kategori, site, plotter, folyo kesim, ümraniye";
            model.Canonical = $"{Core.UrlExtensions.SiteUrl}/kategori/1";
            model.CreatedDateTime = categoryList.OrderBy(x => x.CreatedDateTime).FirstOrDefault().CreatedDateTime;
            model.UpdatedDateTime = categoryList.OrderByDescending(x => x.UpdateDateTime).FirstOrDefault().UpdateDateTime;
            model.Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{settingService.GetLastFirmId().Logo}";


            return View(model);
        }
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult List(Guid? contentId, int page = 1, string q = null)
        {
            var model = new SiteModel();

            if (contentId.HasValue)
            {

                var category = categoryService.GetCategoryList().FirstOrDefault(x => x.ContentId == contentId.Value);

                var productList = productService.GetProductList().Where(x => x.CategoryId == category.Id);
                if (!string.IsNullOrEmpty(q))
                {
                    switch (q)
                    {
                        case "u":
                            productList = productList.OrderBy(x => x.SalesPrice);
                            break;
                        case "p":
                            productList = productList.OrderByDescending(x => x.SalesPrice);
                            break;
                        case "a":
                            productList = productList.OrderBy(x => x.Name);
                            break;
                        case "z":
                            productList = productList.OrderByDescending(x => x.Name);
                            break;
                        default:
                            break;
                    }


                }
                int pageSize = WebCompany.Core.UrlExtensions.PageSize;
                int pageCount = productList.Count();
                model.PageSize = pageSize;
                model.PageCount = pageCount;
                model.Page = page;
                model.Pages = Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize))) == 0 ? 1 : Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize)));
                model.ProductListModel = productList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                model.CategoryModel = category;
                model.PageListModel = pageService.GetPageList().Where(x => x.IsHomePage).ToList();
                model.CategoryListModel = categoryService.GetCategoryList().ToList();
                //var metaContent = metaTagService.GetMetaTagContentId(contentId.Value);
                model.SetMetaTagViewModel(new MetaTagViewModel()
                {
                    MetaTitle = category.MetaTitle,
                    MetaDescription = category.MetaDescription,
                    MetaKeywords = category.MetaKeywords,
                    //Canonical = WebCompany.Core.UrlService.WebViewUrl(category.Url),
                    CreatedDateTime = category.CreatedDateTime,
                    UpdatedDateTime = category.UpdateDateTime,
                    Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{settingService.GetLastFirmId().Logo}",
                });
                return View(model);
            }
            return RedirectPermanent("/");
        }
    }
}