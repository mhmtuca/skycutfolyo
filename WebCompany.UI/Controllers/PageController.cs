﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core.Tools;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class PageController : Controller
    {
        private IProductService productService;
        private IPageService pageService;
        private IMetaTagService metaTagService;
        public PageController(IProductService productService, 
            IPageService pageService, 
            IMetaTagService metaTagService)
        {
            this.productService = productService;
            this.pageService = pageService;
            this.metaTagService = metaTagService;
        }
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(Guid? contentId)
        {
            if (contentId.HasValue)
            {               
                var model = new SiteModel();
                var page= pageService.GetPageContentId(contentId.Value);
                model.PageModel = page;
                model.PageListModel = pageService.GetPageList();
                

                //model.MetaTagViewModel = new MetaTagViewModel() {
                //    Canonical = WebCompany.Core.UrlService.WebViewUrl(page.Url),
                //    ContentId=meta.ContentId,
                //    CreatedDateTime=meta.CreatedDateTime,
                //    Img = $"{Core.UrlExtensions.SiteUrl}/content/img/p/desktop/{page.Image}",
                //    MetaDescription = meta.MetaDescription,
                //    MetaKeywords = meta.MetaKeywords,
                //    MetaTitle = meta.MetaTitle,
                //    UpdatedDateTime =meta.UpdateDateTime
                //};
                
                return View(model);
            }
            return RedirectPermanent("/"); 

        }
    }
}