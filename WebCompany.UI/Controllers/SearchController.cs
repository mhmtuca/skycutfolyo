﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class SearchController : Controller
    { 
        private readonly IProductService productService;
        private readonly IPageService pageService;
        private readonly ISearchKeywordService searchKeywordService;
        private readonly ISettingService _settingService;
        public SearchController(IProductService productService, 
            IPageService pageService, 
            ISearchKeywordService searchKeywordService,
            ISettingService settingService)
        { 
            this.productService = productService;
            this.pageService = pageService;
            this.searchKeywordService = searchKeywordService;
            _settingService = settingService;
        }
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(string q,int s=1)
        {
            var model = new SiteModel();
            if (string.IsNullOrEmpty(q))
            {
                Response.Clear();
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", Core.UrlExtensions.SiteUrl);
                Response.End();
            }
            else
            {
               
                var result = searchKeywordService.GetSearchKeywordList().Where(x => x.Search.Contains(q));
                var products = new List<ProductsDto>();
          
                foreach (var item in result)
                {
                    var product = productService.GetProductGuidId(item.ContentId);
                    if (product != null)
                    {
                        products.Add(product);
                    }
                     
                }
                 
                int pageSize = WebCompany.Core.UrlExtensions.PageSize;
                int pageCount = products.Count();
                model.PageSize = pageSize;
                model.PageCount = pageCount;
                model.Page = s;
                model.Pages = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(pageCount / pageSize))) == 0 ? 1 : Convert.ToInt16(Math.Ceiling(Convert.ToDecimal(pageCount / pageSize)));
                model.ProductListModel = products.Skip((s - 1) * pageSize)?.Take(pageSize).ToList(); 
         

                model.SetMetaTagViewModel(new MetaTagViewModel()
                {
                    Canonical = $"{Core.UrlService.SiteUrl}/arama?q={q}", 
                    CreatedDateTime = DateTime.Now,  
                    Img = $"{Core.UrlExtensions.SiteUrl}/{Core.UrlExtensions.ImageViewPath}/desktop/{_settingService.GetLastFirmId().Logo}",
                    MetaDescription = $"Sitedeki tüm {q} ile ilgili içerikler",
                    MetaKeywords = $"{q} ürün,arama, sonuç, bulma, kelimesi, nedir, anlamı, kategori, blog,site,vs",
                    MetaTitle = $"{q} arama sonuçları | {Core.UrlExtensions.SiteName } ",
                    UpdatedDateTime = DateTime.Now,
                });
                ViewData["Search"] = q;
                return View(model);
            }
            return View(model);
        }
    }
}