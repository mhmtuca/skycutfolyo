﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class SharedController : Controller
    {
        private ISliderService sliderService;
        private IMetaTagService metaTagService;
        private IMenuService menuService;
        private IPageService pageService;
        private ISettingService settingService;
        public SharedController(
        ISliderService sliderService,
        IMetaTagService metaTagService,
        IMenuService menuService,
        IPageService pageService,
        ISettingService settingService
        )
        {
            this.sliderService = sliderService;
            this.metaTagService = metaTagService;
            this.menuService = menuService;
            this.pageService = pageService;
            this.settingService = settingService;
        }

        [ChildActionOnly] 
        public ActionResult Top()
        {
            var model = new SiteModel();
            model.SettingModel = settingService.GetSettingListId();
            return PartialView(model);
        }

        [ChildActionOnly] 
        public ActionResult Slider()
        {
            var model = new SiteModel();
            model.SliderListModel = sliderService.GetSliderList();
            model.SettingModel = settingService.GetSettingListId();
            return PartialView("~/Views/Shared/Layout/Slider.cshtml", model);
        }

        [ChildActionOnly]
        public ActionResult Footer()
        {
            var model = new SiteModel();
            model.SettingModel = settingService.GetSettingListId();
            return PartialView("~/Views/Shared/Layout/Footer.cshtml", model);
        }

        [ChildActionOnly] 
        public ActionResult Header()
        {
            var model = new SiteModel();
            int typeId = (int)ContentType.CompanySite;
            if (Request.Url.PathAndQuery.LastOrDefault() != null && Request.Url.PathAndQuery.Length == 36)
            {
                Guid ContentId = new Guid(Request.Url.PathAndQuery.Split('/').LastOrDefault().ToString().ToString());
                //model.MetaTagModel = metaTagService.GetMetaTagContentId(ContentId);
                return PartialView(model);
            } 
            return PartialView(model); 
        }
        [ChildActionOnly]
        public ActionResult PageContact()
        {
            var model = new SiteModel();
            model.SettingModel = settingService.GetSettingListId();
            return PartialView("~/Views/Shared/Layout/PageContact.cshtml", model);
        }


        [ChildActionOnly]
        public ActionResult Navigation()
        {

            var model = new SiteModel();
            string menu = "";
            var menuList = menuService.GetMenuList();
            foreach (var item in menuList.Where(x => x.MenuTop == 0).OrderBy(x => x.Rows))
            {
                var IsMenu = menuList.Any(f => f.MenuTop == item.Id);
                if (item.MenuTop ==0  && IsMenu)
                {
                    menu += "<li class='nav-item dropdown'>";
                    menu += $"<a class='nav-link dropdown-toggle' href='{WebCompany.Core.UrlService.WebViewUrl(item.MenuUrl.Url)}' id='navbarDropdownPortfolio' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
                    menu += $"{item.MenuUrl.Title}</a>";
                    menu += $"{SubCategoryService(item.Id)}";
                    menu += "</li>";
                    
                }
                else
                {
                    menu += "<li class='nav-item'>";
                    menu += $"<a class='nav-link' href='{WebCompany.Core.UrlService.WebViewUrl(item.MenuUrl.Url)}' title='{item.MenuUrl.Title}'>{item.MenuUrl.Title}</a>";
                    menu += "</li>";
                }
              
            }
            ViewBag.Tree = menu;
            model.SettingModel = settingService.GetLastFirmId();
            return PartialView("~/Views/Shared/Layout/Navigation.cshtml", model);
        }
        public string SubCategoryService(int Id)
        {
            var menuservis = menuService.GetMenuList().Where(x => x.MenuTop == Id);
            StringBuilder str = new StringBuilder();
            if (menuservis.Count() != 0)
            { 
                if(!string.IsNullOrEmpty(menuservis?.FirstOrDefault()?.MenuUrl.Url) )
                str.Append("<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdownPortfolio'>");
                else
                 str.Append("<div class='dropdown-item' aria-labelledby='navbarDropdownPortfolio'>");
                foreach (var item in menuservis)
                {
                    str.Append($"<a class='dropdown-item' href='{WebCompany.Core.UrlService.WebViewUrl(item.MenuUrl.Url)}' title='{item.MenuUrl.Title}'>{item.MenuUrl.Title}</a>");
                    str.Append(SubCategoryService(item.Id));
                }
                str.Append("</div>");
            }

            return str.ToString();
        }

    }
}