﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Core.Tools;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Controllers
{
    public class ProductController : Controller
    { 
        private IProductService productService;
        private IPageService pageService;
        private IMetaTagService metaTagService;
        private ICategoryService categoryService;
        public ProductController(
        IProductService productService,
        IPageService pageService,
        IMetaTagService metaTagService,
        ICategoryService categoryService)
        { 
            this.productService = productService;
            this.pageService = pageService;
            this.metaTagService = metaTagService;
            this.categoryService = categoryService;
        }
        [WhitespaceFilterAttribute]
        [OutputCache(CacheProfile = "Cache1Hour")]
        public ActionResult Index(Guid? contentId)
        {
            if (contentId.HasValue)
            {
                var model = new SiteModel();
                var product = productService.GetProductGuidId(contentId.Value);
                model.ProductModel = product;
                model.CategoryListModel = categoryService.GetCategoryList();
               // var meta = metaTagService.GetMetaTagContentId(contentId.Value);

                model.SetMetaTagViewModel(new MetaTagViewModel()
                {
                   // Canonical = WebCompany.Core.UrlService.WebViewUrl(product.Url),
                    ContentId = product.ContentId,
                    CreatedDateTime = product.CreatedDateTime,
                    Img = $"{Core.UrlExtensions.SiteUrl}/content/img/u/desktop/{product.Image}",
                    MetaDescription = product.MetaDescription,
                    MetaKeywords = product.MetaKeywords,
                    MetaTitle = product.MetaTitle,
                    UpdatedDateTime = product.UpdateDateTime
                });
                return View(model);
            }
            return RedirectPermanent("/"); 
        }
    }
}