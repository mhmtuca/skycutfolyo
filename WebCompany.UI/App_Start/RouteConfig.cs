﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebCompany.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.AppendTrailingSlash = true;
            routes.LowercaseUrls = true;
            routes.MapRoute(
             name: "siteMap",
             url: "sitemap",
             defaults: new { controller = "Home", action = "SiteMap", contentId = UrlParameter.Optional }
             , namespaces: new[] { "WebCompany.UI.Controllers" }
         );
            routes.MapRoute(
            name: "urun",
            url: "urun/{title}/{contentId}",
            defaults: new { controller = "Product", action = "Index", contentId = UrlParameter.Optional }
            , namespaces: new[] { "WebCompany.UI.Controllers" }
             );
            routes.MapRoute(
            name: "urun2",
            url: "{title}/{title2}/urun/{contentId}",
            defaults: new { controller = "Product", action = "Index", contentId = UrlParameter.Optional }
            , namespaces: new[] { "WebCompany.UI.Controllers" }
             );
            routes.MapRoute(
           name: "page",
           url: "p/{title}/{contentId}",
           defaults: new { controller = "Page", action = "Index", contentId = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
            );

            routes.MapRoute(
            name: "kategoriz",
            url: "kategori",
            defaults: new { controller = "Category", action = "Index", contentId = UrlParameter.Optional }
            , namespaces: new[] { "WebCompany.UI.Controllers" }
             );
            routes.MapRoute(
           name: "documents",
           url: "belgeler",
           defaults: new { controller = "Document", action = "List", contentId = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
            );
            routes.MapRoute(
          name: "documenten", 
           url: "document",
           defaults: new { controller = "Document", action = "List", contentId = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
            );
            routes.MapRoute(
           name: "kategoris",
           url: "c/{title}/{contentId}/{page}",
           defaults: new { controller = "Category", action = "List", contentId = UrlParameter.Optional, page = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
            );

            routes.MapRoute(
            name: "Search",
            url: "arama/{q}",
            defaults: new { controller = "Search", action = "Index", q = UrlParameter.Optional },
            namespaces: new[] { "WebCompany.UI.Controllers" }
             );


            routes.MapRoute(
              name: "Blogs",
              url: "Blogs/{page}",
              defaults: new { controller = "Blog", action = "List", page = UrlParameter.Optional }
              , namespaces: new[] { "WebCompany.UI.Controllers" }
               );
            routes.MapRoute(
            name: "Blog",
            url: "Blog/{title}/{contentId}",
            defaults: new { controller = "Blog", action = "Index", contentId = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
           );

            routes.MapRoute(
             name: "Galeri",
             url: "galeri/{page}",
             defaults: new { controller = "Gallery", action = "Index", page = UrlParameter.Optional }
            , namespaces: new[] { "WebCompany.UI.Controllers" }
            );
            routes.MapRoute(
            name: "hakkimizda",
            url: "hakkimizda",
            defaults: new { controller = "Page", action = "Index", page = UrlParameter.Optional }
           , namespaces: new[] { "WebCompany.UI.Controllers" }
           );
            routes.MapRoute(
               name: "aaaa",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
               , namespaces: new[] { "WebCompany.UI.Controllers" }
           );
        }
    }
}
