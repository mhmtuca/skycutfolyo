﻿using System.Web;
using System.Web.Optimization;

namespace WebCompany.UI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        { 
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/assets/css/min/site-min.css"));
             
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Content/assets/js/min/site-min.js")); 

            BundleTable.EnableOptimizations = true;
        }
    }
}
