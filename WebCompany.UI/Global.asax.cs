﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebCompany.Business.AutoMappers;

namespace WebCompany.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas(); 
            RouteConfig.RegisterRoutes(RouteTable.Routes); 
            Infrastructure.BootStrapper.Initialise();
            AutoMapperSite.Run();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           
        }
    }
}
