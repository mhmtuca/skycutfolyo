﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class VideoController : Controller
    {
        #region Members
        private IVideoService _videoService;
        private IAdminServices _adminServices;
        private IMetaTagService _metaTagService;
        private readonly ILanguageService languageService;
        #endregion
        #region Ctr
        public VideoController(
        IVideoService videoService,
        IAdminServices adminServices,
        IMetaTagService metaTagService,
        ILanguageService languageService)
        {
            _videoService = videoService;
            _adminServices = adminServices;
            _metaTagService = metaTagService;
            this.languageService = languageService;
        }
        #endregion
        public ActionResult List()
        {
            var model = new VideoViewModel();
            model.VideoModelList = _videoService.GetVideoList().Select(f=> f.ToModel()).ToList();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new VideoModel();
            model.ContentId = Guid.NewGuid();
            model.LanguageId = 1;
            return View(model);
        }
        public ActionResult Edit(Guid contentId)
        {
            var model = new VideoEditModel(); 
            model.VideoModelList = _videoService.GetVideoListByContentId(contentId).Select(v=> v.ToModel()).ToList();
            model.LanguageListModel = languageService.GetLanguageList().Select(l=> l.ToModel()).ToList(); 
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var video = _videoService.GetVideoById(Id);
            _videoService.Delete(video);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/video/List");
        }
        [ValidateInput(false)]
        public ActionResult Save(VideoModel model)
        {
 
            if (ModelState.IsValid)
            {

                string logoPath = UrlExtensions.VideoPath;
                if (model.Photo != null)
                {

                    var imageConfig = new Config()
                    {
                        FileBase = model.Photo,
                        FileName = model.Photo.FileName,
                        Width = UrlExtensions.VideoWidth,
                        ImagePath = UrlExtensions.VideoPath,
                        ContentName = model.Title
                    };
                    logoPath = ImageUpload.ImgUploadGaleri(imageConfig);

                }
                if (model.Id == 0)
                {
                    model.CreatedDateTime = DateTime.Now;
                    model.UpdateDateTime = DateTime.Now;
                    model.Image = logoPath;
                    model.ContentId = model.ContentId;
                    model.LanguageId = model.LanguageId;
                     
                    var url = UrlService.CreateUrl("v", model.Title, model.ContentId.ToString());
                    model.Url = url;
                    _videoService.Insert(model.ToEntity());
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                }
                else
                {
                    var video = _videoService.GetVideoById(model.Id);

                    video.Image = model.Photo == null ? model.Image : logoPath;
                    video.UpdateDateTime = DateTime.Now;
                    video.Title = model.Title;
                    video.Rows = model.Rows; 
                    video.Actived = model.Actived;
                    video.Detials = model.Detials;
                    video.VideoCode = model.VideoCode;
                    video.ContentId = model.ContentId;
                    var url = UrlService.CreateUrl("v", model.Title, model.ContentId.ToString());
                    video.Url = url;
                    video.MetaDescription = model.MetaDescription;
                    video.MetaKeywords = model.MetaKeywords;
                    video.MetaTitle = model.MetaTitle;
                    video.LanguageId = model.LanguageId; 
                    _videoService.Update(video);
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                }
               
                return Redirect(UrlService.AdminGetUrl("/video/List"));
            }
            return View(model.Id == 0 ? "Create" : "Edit", model);
        }

    }
}