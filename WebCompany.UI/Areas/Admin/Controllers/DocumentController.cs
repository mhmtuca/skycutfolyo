﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;
using static WebCompany.Core.ImageUpload;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class DocumentController : Controller
    {
        #region Members
        private IDocumentService _documentService;
        private IAdminServices _adminServices;
        private IMetaTagService _metaTagService;
        private readonly ILanguageService languageService;
        #endregion
        #region Ctr
        public DocumentController(
        IDocumentService documentService,
        IAdminServices adminServices,
        IMetaTagService metaTagService,
        ILanguageService languageService)
        {
            _documentService = documentService;
            _adminServices = adminServices;
            _metaTagService = metaTagService;
            this.languageService = languageService;
        }
        #endregion
        public ActionResult List()
        {
            var model = new SiteModel();
            model.DocumentModelList = _documentService.GetDocumentList();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new SiteModel();
            var document = new DocumentDto();
            model.DocumentModel = document;
            model.DocumentModel.ContentId = Guid.NewGuid();
            model.DocumentModel.LanguageId = 1;
            return View(model);
        }
        public ActionResult Edit(Guid Id)
        {
            var model = new SiteModel(); 
            model.DocumentModelList = _documentService.GetDocumentListByContentId(Id);
            model.LanguageListModel = languageService.GetLanguageList(); 
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var document = _documentService.GetDocumentById(Id);
            _documentService.Delete(document);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/document/List");
        }
        [ValidateInput(false)]
        public ActionResult Save(SiteModel model, HttpPostedFileBase files, FormCollection frm)
        {
 
            if (ModelState.IsValid)
            {
                var file = new FileUploadModel();
               
                if (files!=null)
                {
                    file = ImageUpload.FileUpload(files);
                    if (file.IsError)
                    {
                        ModelState.AddModelError("Hata", file.FileName);
                    }
                }
               

                if (model.DocumentModel.Id == 0)
                {
                    model.DocumentModel.CreatedDateTime = DateTime.Now;
                    model.DocumentModel.UpdateDateTime = DateTime.Now;
                    model.DocumentModel.Path = file.FileName;
                    model.DocumentModel.ContentId = model.DocumentModel.ContentId; 
                    _documentService.Insert(model.DocumentModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                }
                else
                {
                    var document = _documentService.GetDocumentById(model.DocumentModel.Id);

                    document.Path = file == null ? model.DocumentModel.Path : file.FileName;
                    document.UpdateDateTime = DateTime.Now;
                    document.Title = model.DocumentModel.Title;
                    document.Rows = model.DocumentModel.Rows; 
                    document.Actived = model.DocumentModel.Actived; 
                    document.ContentId = model.DocumentModel.ContentId;
                    document.LanguageId = model.DocumentModel.LanguageId; 
                    _documentService.Update(document);
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                }
                
                return Redirect(UrlService.AdminGetUrl("document/List"));
            }
            return View(model.DocumentModel.Id == 0 ? "Create" : "Edit", model);
        }

    }
}