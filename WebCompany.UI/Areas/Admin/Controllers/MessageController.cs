﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class MessageController : Controller
    {
        #region Members

        private readonly IMessageService _messageService;

        #endregion

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }
        // GET: Admin/Message
        public ActionResult List(int page=1)
        {
            var messageList =_messageService.GetMessageList();
            var model = new SiteModel();
            model.MessageModelList = messageList.Skip((page-1)* WebCompany.Core.UrlExtensions.PageSize).Take(WebCompany.Core.UrlExtensions.PageSize).ToList();
            model.Page = page;
            model.PageCount = messageList.Count();
            model.PageSize = WebCompany.Core.UrlExtensions.PageSize;
            model.Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(messageList.Count()) / Convert.ToDecimal(WebCompany.Core.UrlExtensions.PageSize)));
            return View(model);
        }

        public ActionResult Reader(int id = 1)
        {
            var message = _messageService.GetMessageId(id);
            message.Actived = message.Actived == true ? false : true;
            _messageService.Update(message);
            TempData["Message"] = "Müşteri mesajı durumu güncellenmiştir.";
            TempData["Result"] = true;
            return Redirect("/Admin/Message/List"); 
        }
    }
}