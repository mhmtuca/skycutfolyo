﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class PageController : Controller
    {
        private IMetaTagService metaTagService;
        private ISearchKeywordService searchKeywordService;
        private IPageService pageService;
        private IAdminServices adminServices;
        private IPageContentService _PageContentServices;
        private ILanguageService languageService;
        public PageController(
        IMetaTagService metaTagService,
        ISearchKeywordService searchKeywordService,
        IPageService pageService,
        IAdminServices adminServices,
        IPageContentService PageContentServices,
        ILanguageService languageService
        )
        {
            this.metaTagService = metaTagService;
            this.searchKeywordService = searchKeywordService;
            this.pageService = pageService;
            this.adminServices = adminServices;
            this._PageContentServices = PageContentServices;
            this.languageService = languageService;
        }
        public ActionResult List()
        {
            var model = new SiteModel();
            model.PageListModel = pageService.GetPageList();
            model.PageContentList = _PageContentServices.GetPageContentList();
            return View(model);
        }

        public ActionResult Create()
        {
            var model = new SiteModel();
            var page = new PagesDto();
            model.PageModel = page;
            model.PageModel.ContentId = Guid.NewGuid();
            model.PageModel.LanguageId = 1;
            return View(model);
        }
        public ActionResult Edit(Guid contentId)
        {
            var model = new SiteModel();

            model.PageListModel = pageService.GetPageListByContentId(contentId);
            model.LanguageListModel = languageService.GetLanguageList();

            model.PageContentList = _PageContentServices.GetPageContentList().Where(d => d.ContentId == contentId).ToList();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var page = pageService.GetPageId(Id);
            pageService.Delete(page);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/page/List");
        }

        [ValidateInput(false)]
        public ActionResult Save(SiteModel model)
        {
            int pageId = 1;
            Guid ContentId = Guid.NewGuid();

            string logoName = model?.PageModel?.Image;

            if (model.PageModel.FileUpload != null)
            {
                var imageConfig = new Config()
                {
                    FileBase = model.PageModel.FileUpload,
                    FileName = model.PageModel.FileUpload.FileName,
                    Width = UrlExtensions.PageImageWidth,
                    ImagePath = UrlExtensions.PageImagePath,
                    ContentName = model.PageModel.Title
                };
                logoName = ImageUpload.ImgUploadGaleri(imageConfig);

            }
            else
            {
                logoName = UrlExtensions.PageNotFound;
            }


            if (model.PageModel.Id == 0)
            {
                model.PageModel.CreatedDateTime = DateTime.Now;
                model.PageModel.UpdateDateTime = DateTime.Now;
                model.PageModel.Url = UrlService.CreateUrl("p", model.PageModel.Title, model.PageModel.ContentId.ToString());
                model.PageModel.Image = logoName;
                model.PageModel.ViewCount = 1;
                pageService.Insert(model.PageModel);
                ModelState.Clear();
                TempData["Result"] = true;
                TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";


            }
            else
            {
                model.PageModel.UpdateDateTime = DateTime.Now;
                model.PageModel.CreatedDateTime = DateTime.Now;
                model.PageModel.Url = UrlService.CreateUrl("p", model.PageModel.Title, model.PageModel.ContentId.ToString());
                model.PageModel.Image = logoName == UrlExtensions.ProductNotFound && model.PageModel.Image != null ? model.PageModel.Image : logoName;
                model.PageModel.ViewCount = 1;
                pageService.Update(model.PageModel);
                ModelState.Clear();
                TempData["Result"] = true;
                TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                pageId = model.PageModel.Id;
                ContentId = model.PageModel.ContentId;

            }
            //SearchKeywords 
            var searchKeywords = new SearchKeywordsDto()
            {
                ContentId = ContentId,
                Search = $"{model.PageModel.Title} , { (model.PageModel.ShortDetail.Length > 155 ? model.PageModel.ShortDetail.Substring(0, 155).Replace(" ", " , ") : model.PageModel.ShortDetail.Replace(" ", " , "))   }  ",
                ContentTypeId = (int)ContentType.Page,
                Deleted = false
            };
            //Keywords Ekleme
            //adminServices.AddSearchKeywords(searchKeywords);

            //Meta Tag Ekle

            //adminServices.AddMetaTag(frm, ContentId, model.PageModel.Title, model.PageModel.ShortDetail, ContentType.Page);
            return Redirect(UrlService.AdminGetUrl("page/List"));



        }

        [ValidateInput(false)]
        public ActionResult PageContentSave(SiteModel model)
        {

            string logoName = "";
            if (model.PageContent.FileUpload != null)
            {
                var imageConfig = new Config()
                {
                    FileBase = model.PageContent.FileUpload,
                    FileName = model.PageContent.FileUpload.FileName,
                    Width = UrlExtensions.PageImageWidth,
                    ImagePath = UrlExtensions.PageImagePath,
                    ContentName = model.PageContent.ContentTitle
                };
                logoName = ImageUpload.ImgUploadGaleri(imageConfig);

            }
            else
            {
                logoName = UrlExtensions.PageNotFound;
            }

            model.PageContent.CreatedDateTime = DateTime.Now;
            model.PageContent.UpdateDateTime = DateTime.Now;
            model.PageContent.ContentText = model.PageContent.ContentType == 1 ? model.PageContent.ContentText : (model.PageContent.FileUpload != null ? logoName : model.PageContent.ContentText);
            model.PageContent.ContentDetail = model.PageContent.ContentType == 1 ? "" : model.PageContent.ContentDetail;
            _PageContentServices.Insert(model.PageContent);
            ModelState.Clear();
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";

            return Redirect("/admin/page/edit/" + model.PageContent.ContentId);

        }
        public ActionResult ContentDelete(int Id)
        {
            var page = _PageContentServices.GetPageContentById(Id);
            var contentId = page.ContentId;
            _PageContentServices.Delete(page);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/page/edit/"+ contentId);
        }
        [ValidateInput(false)]
        public ActionResult PageAllContentSave(List<PageContentDto> PageContentList)
        {


            string logoName = "";


            if (PageContentList.Any())
            {
                foreach (var model in PageContentList)
                {
                    if (model != null)
                    {
                        if (model.FileUpload != null)
                        {
                            var imageConfig = new Config()
                            {
                                FileBase = model.FileUpload,
                                FileName = model.FileUpload.FileName,
                                Width = UrlExtensions.PageImageWidth,
                                ImagePath = UrlExtensions.PageImagePath,
                                ContentName = model.ContentTitle
                            };
                            logoName = ImageUpload.ImgUploadGaleri(imageConfig);

                        }
                        else
                        {
                            logoName = UrlExtensions.PageNotFound;
                        }

                    }
                    model.CreatedDateTime = DateTime.Now;
                    model.UpdateDateTime = DateTime.Now;
                    model.ContentText = model.ContentType == 1 ? model.ContentText : (model.FileUpload != null ? logoName : model.ContentText);
                    model.ContentDetail = model.ContentType == 1 ? "" : model.ContentDetail;

                    _PageContentServices.Update(model);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";

                }


            }
            return Redirect("/admin/page/edit/" + PageContentList.FirstOrDefault().PageContentId);
            //else
            //{
            //    model.PageContent.CreatedDateTime = DateTime.Now;
            //    model.PageContent.UpdateDateTime = DateTime.Now;
            //    model.PageContent.ContentText = logoName[0] == UrlExtensions.ProductNotFound && model.PageContent.ContentText != null ? model.PageContent.ContentText : logoName[0];

            //    _PageContentServices.Update(model.PageContent);
            //    ModelState.Clear();
            //    TempData["Result"] = true;
            //    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
            //}



            //return View(model.PageContent.Id == 0 ? "Create" : "Edit", model);

        }
    }
}