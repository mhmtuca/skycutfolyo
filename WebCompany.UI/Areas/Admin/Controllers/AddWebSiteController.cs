﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class AddWebSiteController : Controller
    {
        #region Members
        private ISettingService SettingService;
        private IMetaTagService metaTagService;
        private IAdminServices adminServices;
        private IUrlsService urlsService;



        #endregion
        #region Ctr
        public AddWebSiteController(ISettingService SettingService, IMetaTagService metaTagService, IAdminServices adminServices, IUrlsService urlsService)
        {
            this.SettingService = SettingService;
            this.metaTagService = metaTagService;
            this.adminServices = adminServices;
            this.urlsService = urlsService;
        }
        #endregion
        // GET: Admin/AddWebSite
        public ActionResult Index()
        {

            var model = new AddSiteModel();
            model.ContentId = Guid.NewGuid();
            model.LanguageId = 1;
            return View(model);

        }

        public ActionResult Save(AddSiteModel model)
        {
            Guid ContentId = Guid.NewGuid();
            if (!ModelState.IsValid)
                return View("Index", model);

            string logoPath = null;
            if (model.Image != null)
            {
                var config = new Config()
                {
                    FileBase = model.Image,
                    FileName = model.Image.FileName,
                    ContentName = "Logo",
                    ImagePath = UrlExtensions.ImagePath,
                    Width = UrlExtensions.ImagePathWidth
                };
                logoPath = ImageUpload.ImgUploadGaleri(config);
            }


            model.Actived = true;
            model.Deleted = false;
            model.CreatedDateTime = DateTime.Now;
            model.UpdateDateTime = DateTime.Now;
            model.Logo = logoPath;
            SettingService.Insert(model.ToEntity());

            //Add menu 
         
            foreach (var item in GetUrlList())
            {
                urlsService.Insert(item);
            }
          

           
            //adminServices.AddMetaTag(frm, model.SettingModel.ContentId != null ? model.SettingModel.ContentId : ContentId, model.SettingModel.Title, model.SettingModel.Title);
            return RedirectToAction("/ResultSite");

        }

        public List<UrlsDto> GetUrlList()
        {
            List<UrlsDto> UrlList = new List<UrlsDto>();
            //blog
            var blog = new UrlsDto();
            blog.Actived = true;
            blog.Title = "blog";
            blog.ContentId = Guid.NewGuid();
            blog.CreatedDateTime = DateTime.Now;
            blog.UpdateDateTime = DateTime.Now;
            blog.Deleted = false;
            blog.LanguageId = 1;
            blog.Url = "blog";
            UrlList.Add(blog);
            var blogen = new UrlsDto();
            blogen = blog;
            blogen.Title = "blog";
            blog.LanguageId = 2;
            UrlList.Add(blogen);

            //Galeri
            var galeri = new UrlsDto();
            galeri.Actived = true;
            galeri.Title = "galeri";
            galeri.ContentId = Guid.NewGuid();
            galeri.CreatedDateTime = DateTime.Now;
            galeri.UpdateDateTime = DateTime.Now;
            galeri.Deleted = false;
            galeri.LanguageId = 1;
            galeri.Url = "gallery";
            UrlList.Add(galeri);
            var galerien= new UrlsDto();
            galerien = galeri;
            galerien.Title = "gallery";
            galerien.LanguageId = 2;
            galerien.Url = "gallery";
            UrlList.Add(galerien);
            //Belge
            var document = new UrlsDto();
            document.Title = "belgeler";
            document.Actived = true;
            document.ContentId = Guid.NewGuid();
            document.CreatedDateTime = DateTime.Now;
            document.UpdateDateTime = DateTime.Now;
            document.Deleted = false;
            document.LanguageId = 1;
            document.Url = "document";
            UrlList.Add(document);
            var documenten = new UrlsDto();
            documenten.Title = "document";
            documenten.Actived = true;
            documenten.ContentId = Guid.NewGuid();
            documenten.CreatedDateTime = DateTime.Now;
            documenten.UpdateDateTime = DateTime.Now;
            documenten.Deleted = false;
            documenten.LanguageId = 1;
            documenten.Url = "document";
            UrlList.Add(documenten);

            //Video
            var video = new UrlsDto();
            video.Title = "videolar";
            video.Actived = true;
            video.ContentId = Guid.NewGuid();
            video.CreatedDateTime = DateTime.Now;
            video.UpdateDateTime = DateTime.Now;
            video.Deleted = false;
            video.LanguageId = 1;
            video.Url = "videolar";
            UrlList.Add(video);
            var videoen = new UrlsDto();
            videoen = video;
            videoen.Title = "video";
            videoen.LanguageId = 2;
            videoen.Url = "videos";
            UrlList.Add(videoen);

            return UrlList; 
        }
        public ActionResult ResultSite()
        {

            var company = SettingService.GetLastFirmId();
            var list = new Dictionary<string, string>();
            list.Add("SiteUrl", "http://www.site.com");
            list.Add("SiteId", company?.Id.ToString());
            list.Add("SiteContentId", company?.ContentId.ToString());
            list.Add("SiteName", company?.Title.ToString()); 
            ViewBag.WebConfigList = list;
            return View();
        }
    }
}