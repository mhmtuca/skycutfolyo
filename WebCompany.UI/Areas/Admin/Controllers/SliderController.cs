﻿using System;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services; 
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        private ISliderService sliderService;
        private readonly ILanguageService languageService;
        public SliderController(
        ISliderService sliderService,
        ILanguageService languageService
        )
        {
            this.sliderService = sliderService;
            this.languageService = languageService;
        }
        public ActionResult List()
        {
            var model = new SiteModel();
            model.SliderListModel = sliderService.GetSliderList();
            return View(model);
        }

        public ActionResult Create()
        {
            var model = new SiteModel();
            model.SliderModel = new SliderDto();
            model.SliderModel.ContentId = Guid.NewGuid();
            model.SliderModel.LanguageId = 1;
            return View(model);

        }
        public ActionResult Edit(Guid Id)
        {
            var model = new SiteModel();
            model.SliderListModel = sliderService.GetSliderListByContentId(Id);
            model.LanguageListModel = languageService.GetLanguageList();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var slider = sliderService.GetSliderId(Id);
            sliderService.Delete(slider);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/slider/List");
        }

        public ActionResult Save(SiteModel model, HttpPostedFileBase file)
        {
             
            if (ModelState.IsValid)
            {

                string sliderPath = UrlExtensions.ProductNotFound;
                if (file != null)
                {
                    var imageConfig = new Config()
                    {
                        FileBase = file,
                        FileName = file.FileName,
                        Width = UrlExtensions.SliderImageWidth,
                        ImagePath = UrlExtensions.SliderImagePath,
                        ContentName = model.SliderModel.Name
                    };
                    sliderPath = ImageUpload.ImgUploadGaleri(imageConfig); 

                }

                if (model.SliderModel.Id == 0)
                {

                    model.SliderModel.CreatedDateTime = DateTime.Now;
                    model.SliderModel.UpdateDateTime = DateTime.Now;
                    model.SliderModel.Image = sliderPath;
                    sliderService.Insert(model.SliderModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                    return Redirect(UrlService.AdminGetUrl("slider/List"));
                }
                else
                {

                    model.SliderModel.CreatedDateTime = DateTime.Now;
                    model.SliderModel.UpdateDateTime = DateTime.Now;
                    model.SliderModel.Image = file != null ? sliderPath:model.SliderModel.Image;
                    sliderService.Update(model.SliderModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellemiştir";
                    return Redirect(UrlService.AdminGetUrl("slider/List"));
                }
            }
            return View(model.SliderModel.Id == 0 ? "Create" : "Edit");

        }
    }
}