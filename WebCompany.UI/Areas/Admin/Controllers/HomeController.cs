﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Services;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private IMetaTagService _metaTagService;
        public HomeController(IMetaTagService metaTagService)
        {
            _metaTagService = metaTagService;
        }
        // GET: Admin/Home
        public ActionResult Index()
        {
            //_metaTagService.GetMetaTagList();
            return View();
        }
    }
}