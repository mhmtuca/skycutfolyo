﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class BlogController : Controller
    {
        #region Members
        private IBlogService _blogService;
        private IAdminServices _adminServices;
        private IMetaTagService _metaTagService;
        private readonly ILanguageService languageService;
        #endregion
        #region Ctr
        public BlogController(
        IBlogService blogService,
        IAdminServices adminServices,
        IMetaTagService metaTagService,
        ILanguageService languageService)
        {
            _blogService = blogService;
            _adminServices = adminServices;
            _metaTagService = metaTagService;
            this.languageService = languageService;
        }
        #endregion
        public ActionResult List()
        {
            var model = new SiteModel();
            model.BlogModelList = _blogService.BlogList();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new SiteModel();
            var blog = new BlogDto();
            model.BlogModel = blog;
            model.BlogModel.ContentId = Guid.NewGuid();
            model.BlogModel.LanguageId = 1;
            return View(model);
        }
        public ActionResult Edit(Guid Id)
        {
            var model = new SiteModel(); 
            model.BlogModelList = _blogService.BlogList(Id);
            model.LanguageListModel = languageService.GetLanguageList(); 
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var blog = _blogService.GetBlogId(Id);
            _blogService.Delete(blog);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/blog/List");
        }
        [ValidateInput(false)]
        public ActionResult Save(SiteModel model, HttpPostedFileBase photo, FormCollection frm)
        {
 
            if (ModelState.IsValid)
            {

                string logoPath = UrlExtensions.BlogNotFound;
                if (photo != null)
                {

                    var imageConfig = new Config()
                    {
                        FileBase = photo,
                        FileName = photo.FileName,
                        Width = UrlExtensions.BlogWidth,
                        ImagePath = UrlExtensions.BlogPath,
                        ContentName = model.BlogModel.Title
                    };
                    logoPath = ImageUpload.ImgUploadGaleri(imageConfig);

                }
                if (model.BlogModel.Id == 0)
                {
                    model.BlogModel.CreatedDateTime = DateTime.Now;
                    model.BlogModel.UpdateDateTime = DateTime.Now;
                    model.BlogModel.Path = logoPath;
                    model.BlogModel.ContentId = model.BlogModel.ContentId;
                    var url = UrlService.CreateUrl("b", model.BlogModel.Title, model.BlogModel.ContentId.ToString());
                    model.BlogModel.Url = url;
                    _blogService.Insert(model.BlogModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                }
                else
                {
                    var blog = _blogService.GetBlogId(model.BlogModel.Id);

                    blog.Path = photo == null ? model.BlogModel.Path : logoPath;
                    blog.UpdateDateTime = DateTime.Now;
                    blog.Title = model.BlogModel.Title;
                    blog.Rows = model.BlogModel.Rows;
                    blog.IsHomePage = model.BlogModel.IsHomePage;
                    blog.Actived = model.BlogModel.Actived;
                    blog.Detail = model.BlogModel.Detail;
                    blog.ShortDetail = model.BlogModel.ShortDetail;
                    blog.ContentId = model.BlogModel.ContentId;
                    var url = UrlService.CreateUrl("b", model.BlogModel.Title, model.BlogModel.ContentId.ToString());
                    blog.Url = url;
                    blog.MetaDescription = model.BlogModel.MetaDescription;
                    blog.MetaKeywords = model.BlogModel.MetaKeywords;
                    blog.MetaTitle = model.BlogModel.MetaTitle;
                    blog.LanguageId = model.BlogModel.LanguageId; 
                    _blogService.Update(blog);
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                }
                //SearchKeywords 
                var searchKeywords = new SearchKeywordsDto()
                {
                    ContentId = model.BlogModel.ContentId,
                    Search = $"{model.BlogModel.Title} , { (model.BlogModel.ShortDetail.Length > 155 ? model.BlogModel.ShortDetail.Substring(0, 155).Replace(" ", " , ") : model.BlogModel.ShortDetail.Replace(" ", " , "))   }  ",
                    ContentTypeId = (int)ContentType.Blog,
                    Deleted = false
                };
                //Keywords Ekleme
              // _adminServices.AddSearchKeywords(searchKeywords);

                //Meta Tag Ekle 
               // _adminServices.AddMetaTag(frm, model.BlogModel.ContentId, model.BlogModel.Title, model.BlogModel.ShortDetail, ContentType.Blog);
                return Redirect(UrlService.AdminGetUrl("blog/List"));
            }
            return View(model.BlogModel.Id == 0 ? "Create" : "Edit", model);
        }

    }
}