﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
        private readonly IMenuService menuService;
        private readonly IUrlsService urlsService;
        private readonly ILanguageService languageService;
        public MenuController(
        IMenuService menuService,
        IUrlsService urlsService,
        ILanguageService languageService)
        {
            this.menuService = menuService;
            this.urlsService = urlsService;
            this.languageService = languageService;
        }
        public ActionResult List()
        {
            //var model = new SiteModel();
            //model.MenuListModel= menuService.GetMenuList(); 
            //return View(model);
            var result = menuService.GetMenuList().OrderBy(x => x.Rows).ToList();

            string menu = "";
            foreach (var item in result.Where(x => x.MenuTop == 0))
            {
                menu += string.Format("<li>{1} - <a href=" + UrlService.AdminGetUrl("Menu/Create/{0}") + ">Ekle</a>", item.Id, item.MenuUrl.Title);
                menu += string.Format("| <a href=" + UrlService.AdminGetUrl("Menu/Edit/{0}") + ">Düzelt</a> ", item.Id);
                menu += string.Format(" | <a href=" + UrlService.AdminGetUrl("Menu/Delete/{0}") + ">Sil</a></li>", item.Id);

                menu += SubCategoryService(item.Id);

            }
            ViewBag.Tree = menu;
            return View();
        }
        public ActionResult Create(int Id = 0)
        {
            var model = new MenuCreateViewModel();
            model.Menu.MenuTop = 0;
            ModelState.Clear();
            if (Id != 0)
            {
                var menuservice = menuService.GetMenuId(Id);
                model.Menu.Id = 0;
                model.Menu.MenuTop = Id; 
            }

            var urlModel= urlsService.GetUrlList(); 
            model.Urls = urlModel.ToList();
            model.Menu.Id = 0; 
            model.Menu.MenuTop = Id; 
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            var model = new MenuCreateViewModel();
            var menus = menuService.GetMenuId(id);
            model.Menu = GetMenuDtoModel(menus);  
            model.Urls = urlsService.GetUrlList();
            return View("Edit", model);

        }
        public ActionResult Delete(int id)
        {
            var modelResult = menuService.GetMenuId(id);

            menuService.Delete(modelResult);
            TempData["Message"] = "Menü alanı silinmiştir";
            TempData["Result"] = true;
            return Redirect(UrlService.AdminGetUrl("Menu/List"));
        }
        public ActionResult Save(MenuCreateViewModel models)
        {

            if (ModelState.IsValid)
            {
                var model = GetMenusModel(models.Menu);

                if (model.Id == 0)
                {
                    model.CreatedDateTime = DateTime.Now;
                    model.UpdateDateTime = DateTime.Now;  
                   
                    menuService.Insert(model);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                    return Redirect(UrlService.AdminGetUrl("menu/List"));  

                }
                else
                {

                    var menu = menuService.GetMenuId(model.Id);
                    menu.Name = models.Menu.Name;
                    menu.Rows = models.Menu.Rows;
                    menu.UpdateDateTime = DateTime.Now; 
                    menu.Actived = models.Menu.Actived;
                    menu.UrlId = models.Menu.UrlId;
                    menuService.Update(menu);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";

                    return Redirect(UrlService.AdminGetUrl("menu/List"));


                }

            }   
            return View(models.Menu.Id == 0 ? "Create" : "Edit", models);
        }
        public MenuDto GetMenusModel(MenuViewModel menuModel)
        {

            var model = new MenuDto()
            {
                Id = menuModel.Id,
                Name = menuModel.Name,
                MenuTop = menuModel.MenuTop, 
                Rows = menuModel.Rows,
                //Url = menuModel.Url,
                Actived = menuModel.Actived,
                Deleted = menuModel.Deleted, 
                CompanyId=menuModel.CompanyId,
                UrlId=menuModel.UrlId, 
            };

            return model;
        }
        public MenuViewModel GetMenuDtoModel(MenuDto menuModel)
        {

            var model = new MenuViewModel()
            {
                Id = menuModel.Id,
                Name = menuModel.Name,
                MenuTop = menuModel.MenuTop, 
                Rows = menuModel.Rows,
                //Url = menuModel.Url,
                Actived = menuModel.Actived,
                Deleted = menuModel.Deleted,
                CompanyId=menuModel.CompanyId, 
                UrlId=menuModel.UrlId
            };

            return model;
        }

       
        public string SubCategoryService(int Id)
        {
            var menuservis = menuService.GetMenuList().Where(x => x.MenuTop == Id);
            StringBuilder str = new StringBuilder();
            if (menuservis.Count() != 0)
            {

                str.Append("<ul>");
                foreach (var item in menuservis)
                {
                    str.Append(string.Format("<li>{0}", item.MenuUrl.Title));
                    
                        str.Append(string.Format("<a href=" + UrlService.AdminGetUrl("Menu/Create/{0}") + ">Ekle</a>", item.Id));
                        str.Append(string.Format("| <a href=" + UrlService.AdminGetUrl("Menu/Edit/{0}") + ">Düzelt</a>", item.Id));
                        str.Append(string.Format(" | <a href=" + UrlService.AdminGetUrl("Menu/Delete/{0}") + "> Sil</a></li>", item.Id));

                    
                    str.Append(SubCategoryService(item.Id));
                }
                str.Append("</ul>");
            }

            return str.ToString();
        }
    }
}