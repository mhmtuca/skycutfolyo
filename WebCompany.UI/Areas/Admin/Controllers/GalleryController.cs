﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class GalleryController : Controller
    {
        #region Members
        private IGalleryService galleryService;
        private ILanguageService languageService;
        private IGalleryImageService galleryImageService;
        #endregion
        #region Ctr
        public GalleryController(ILanguageService languageService, IGalleryService galleryService, IGalleryImageService galleryImageService)
        {
            this.languageService = languageService;
            this.galleryService = galleryService;
            this.galleryImageService = galleryImageService;
        }
        #endregion
        public ActionResult List()
        {
            var model = new SiteModel();
            model.GalleryModelList = galleryService.GalleryList();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new SiteModel();
            var gallery = new GalleryDto();
            model.GalleryModel = gallery;
            model.GalleryModel.ContentId = Guid.NewGuid();
            model.GalleryModel.LanguageId = 1;
            return View(model);
        }

        public ActionResult GalleryImageAdd(int Id)
        {
            var model = new GalleryImageViewModel();
            model.GalleryImageModel = new GalleryImageModel();
           
            var gallery = galleryService.GetGalleryId(Id);
            model.Title = gallery.Title;
            model.Path = gallery.Path;
            model.GalleryId = gallery.Id;
            model.GalleryImageListModel = galleryImageService.GalleryImageList(Id).Select(f=> f.ToModel()).ToList();
            return View(model);
        }

        public ActionResult SaveImage(GalleryImageModel gallery)
        {
            
            if (ModelState.IsValid)
            {

                string logoPath = UrlExtensions.GalleryNotFound;
                if (gallery.File != null)
                {

                    var imageConfig = new Config()
                    {
                        FileBase = gallery.File,
                        FileName = gallery.File.FileName,
                        Width = UrlExtensions.GalleryWidth,
                        ImagePath = UrlExtensions.GalleryPath,
                        ContentName = gallery.Title
                    };
                    logoPath = ImageUpload.ImgUploadGaleri(imageConfig);

                }
                if (gallery.Id == 0)
                {
                    gallery.CreatedDateTime = DateTime.Now;
                    gallery.UpdateDateTime = DateTime.Now;
                    gallery.Path = logoPath; 
                    galleryImageService.Insert(gallery.ToEntity());
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                }
                else
                {
                    var model = galleryImageService.GetGalleryImageById(gallery.Id);

                    model.Path = gallery.File == null ? model.Path : logoPath;
                    model.UpdateDateTime = DateTime.Now;
                    model.Title = gallery.Title;
                    model.Rows = gallery.Rows; 
                    model.Actived = gallery.Actived;
                    galleryImageService.Update(model);
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                }
                return Redirect(UrlService.AdminGetUrl("/Gallery/GalleryImageAdd/" + gallery.GalleryId));
            }
            return View("/admin/Gallery/GalleryImageAdd/"+gallery.GalleryId); 


        }

        public ActionResult Edit(Guid Id)
        {
            var model = new SiteModel();

            model.GalleryModelList = galleryService.GalleryListByContentId(Id);
            model.LanguageListModel = languageService.GetLanguageList();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var gallery = galleryService.GetGalleryId(Id);
            galleryService.Delete(gallery);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect(UrlService.AdminGetUrl("gallery/List"));
        }
        [ValidateInput(false)]
        public ActionResult Save(SiteModel model, HttpPostedFileBase photo, FormCollection frm)
        {
           
            if (ModelState.IsValid)
            {

                string logoPath = UrlExtensions.GalleryNotFound;
                if (photo != null)
                {

                    var imageConfig = new Config()
                    {
                        FileBase = photo,
                        FileName = photo.FileName,
                        Width = UrlExtensions.GalleryWidth,
                        ImagePath = UrlExtensions.GalleryPath,
                        ContentName = model.GalleryModel.Title
                    };
                    logoPath = ImageUpload.ImgUploadGaleri(imageConfig);

                }
                if (model.GalleryModel.Id == 0)
                {
                    model.GalleryModel.CreatedDateTime = DateTime.Now;
                    model.GalleryModel.UpdateDateTime = DateTime.Now; 
                    model.GalleryModel.Path = logoPath;
                    var url = UrlService.CreateUrl("g", model.GalleryModel.Title, model.GalleryModel.ContentId.ToString());
                    model.GalleryModel.Url = url;
                    galleryService.Insert(model.GalleryModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir"; 
                }
                else
                {
                    var gallery = galleryService.GetGalleryId(model.GalleryModel.Id);

                    gallery.Path = photo == null ? model.GalleryModel.Path : logoPath;
                    gallery.UpdateDateTime = DateTime.Now;
                    gallery.Title = model.GalleryModel.Title;
                    gallery.Rows = model.GalleryModel.Rows;
                    gallery.IsHomePage = model.GalleryModel.IsHomePage; 
                    gallery.Actived = model.GalleryModel.Actived;
                    gallery.MetaDescription = model.GalleryModel.MetaDescription;
                    gallery.MetaTitle = model.GalleryModel.MetaTitle;
                    gallery.MetaKeywords = model.GalleryModel.MetaKeywords;
                    var url = UrlService.CreateUrl("g", model.GalleryModel.Title, model.GalleryModel.ContentId.ToString());
                    gallery.Url = url;
                    gallery.ContentId = model.GalleryModel.ContentId;
                    galleryService.Update(gallery); 
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";  
                }  
                return Redirect(UrlService.AdminGetUrl("gallery/List"));
            } 
            return View(model.GalleryModel.Id == 0 ? "Create" : "Edit", model);
        }

    }
}