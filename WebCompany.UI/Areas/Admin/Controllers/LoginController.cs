﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Logout()
        {
            HttpCookie myCookie = new HttpCookie("UserLogin"); 
            myCookie["IsVisit"] = "false";
            myCookie.Expires = DateTime.Now.AddHours(-1); 
            Response.Cookies.Add(myCookie);
            return Redirect("/Admin/Login/Index");
            
        }
        public ActionResult LoginUser(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Email.ToUpper() == Core.UrlExtensions.Email.ToUpper() && model.Password.ToUpper() == Core.UrlExtensions.Password.ToUpper())
                {
 
                    HttpCookie myCookie = new HttpCookie("UserLogin");
                    myCookie["Email"] = model.Email;
                    myCookie["IsVisit"] = "true";
                    myCookie.Expires = DateTime.Now.AddDays(1d);

                    if (model.IsRememberMe)
                    {
                        myCookie.Expires = DateTime.Now.AddYears(1);
                    }
                    else
                    {
                        myCookie.Expires = DateTime.Now.AddHours(1);
                    }
                    Response.Cookies.Add(myCookie);
                    return Redirect("/Admin/Home/Index");
                }
                else
                {
                    ModelState.AddModelError("Error", "Kullanıcı adı ve şifre hatalı");
                    return PartialView("Index");
                }
               
            }
            return PartialView("Index");
        }
    }
}