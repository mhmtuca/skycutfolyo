﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class UrlsController : Controller
    {
        // GET: Admin/Urls
        #region Members
        private readonly IUrlsService urlsService;
        private readonly ILanguageService languageService;
        #endregion
        #region Ctr
        public UrlsController(IUrlsService urlsService, ILanguageService languageService)
        {
            this.urlsService = urlsService;
            this.languageService = languageService;
        }
        #endregion
        public ActionResult List()
        {
            var model = new SiteModel();
            model.UrlsModelList = urlsService.GetUrlList();
            return View(model);
        }
        public ActionResult Create()
        {
            var model = new SiteModel();
            var urls = new UrlsDto();
            urls.ContentId = Guid.NewGuid();
            urls.LanguageId = 1;
            model.UrlsModel = urls;
            return View(model);
        }
        public ActionResult Edit(Guid Id)
        {
            var model = new SiteModel();

            model.UrlsModelList = urlsService.GetUrlListByContentId(Id);
            model.LanguageListModel = languageService.GetLanguageList();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var urls = urlsService.GetUrBylId(Id);
            urlsService.Delete(urls);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect(UrlService.AdminGetUrl("urls/List"));
        }
        [ValidateInput(false)]
        public ActionResult Save(SiteModel model)
        {

            if (ModelState.IsValid)
            {
 
                if (model.UrlsModel.Id == 0)
                {
                    model.UrlsModel.CreatedDateTime = DateTime.Now;
                    model.UrlsModel.UpdateDateTime = DateTime.Now;
                    model.UrlsModel.Type =true;
                    model.UrlsModel.Deleted = false;
                    urlsService.Insert(model.UrlsModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";
                }
                else
                {
                    var urls = urlsService.GetUrBylId(model.UrlsModel.Id);

                    urls.UpdateDateTime = DateTime.Now;
                    urls.Title = model.UrlsModel.Title;
                    urls.ContentId = model.UrlsModel.ContentId;
                    urls.LanguageId = model.UrlsModel.LanguageId;
                    urls.Url = model.UrlsModel.Url;
                    urls.Type = true;
                    urls.Deleted = false;
                    urls.Actived = model.UrlsModel.Actived;
                    urlsService.Update(urls);
                    ModelState.Clear();

                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";
                }
                return Redirect(UrlService.AdminGetUrl("urls/List"));
            }
            return View(model.UrlsModel.Id == 0 ? "Create" : "Edit", model);
        }

    }
}