﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        private IMetaTagService metaTagService;
        private ICategoryService categoryService;
        private IProductService productService;
        private IAdminServices adminServices;
        private IUrlsService urlsServices;
        private ILanguageService languageService;
        public CategoryController(
        IMetaTagService metaTagService,
        ICategoryService categoryService, 
        IAdminServices adminServices,
        IProductService productService,
        IUrlsService urlsServices,
        ILanguageService languageService)
        {
            this.metaTagService = metaTagService;
            this.categoryService = categoryService;
            this.adminServices = adminServices;
            this.productService = productService;
            this.urlsServices = urlsServices;
            this.languageService = languageService;
        }
        // GET: Admin/Home
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult Create()
        {
            var model = new SiteModel();
         
            var category = new CategoryDto();
            category.ContentId= Guid.NewGuid();
            category.LanguageId = 1;
            model.CategoryModel = category; 
 
            return View(model);
        }
        public ActionResult List()
        {
            var data = categoryService.GetCategoryList(); 
            var model = adminServices.GetCategoryView(data);
            
            return View(model);
        }
        public ActionResult Edit(Guid id)
        {
            var model = new SiteModel();

            model.CategoryListModel = categoryService.GetCategoryList(id);
            //var MetaTag = metaTagService.GetMetaTagContentId(model.CategoryModel.ContentId);
            //model.MetaTagModel = MetaTag;
            model.LanguageListModel = languageService.GetLanguageList();
            return View("Edit", model);

        }
        public ActionResult Delete(int id)
        {
           
            var category = categoryService.GetCategoryId(id);
            var _productService = productService.GetProductList().Where(x => x.CategoryId == id).ToList();

            TempData["Message"] = $"Silmek istediğiniz kategoriye bağlı {_productService?.Count()} adet ürün bulunmakta.";
            TempData["Result"] = true;
            if (!_productService.Any())
            {
                categoryService.Delete(category);
                TempData["Message"] = "Kayıt silinmiştir.";
                TempData["Result"] = true;
            }
           
            return Redirect(UrlService.AdminGetUrl("Category/List")); 
       
        }
        public ActionResult Save(SiteModel model,FormCollection frm)
        {
             
            if (ModelState.IsValid)
            {
               // adminServices.AddMetaTag(frm, model.CategoryModel.ContentId, model.CategoryModel.Name, model.CategoryModel.Name, ContentType.Blog);
                if (model.CategoryModel.Id == 0)
                {
                    var url = UrlService.CreateUrl("c", model.CategoryModel.Name, model.CategoryModel.ContentId.ToString());
                    model.CategoryModel.CreatedDateTime = DateTime.Now;
                    model.CategoryModel.UpdateDateTime = DateTime.Now; 
                    model.CategoryModel.Url = url;
                    categoryService.Insert(model.CategoryModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";

                    urlsServices.Insert(new UrlsDto()
                    {
                      ContentId=model.CategoryModel.ContentId,
                      Type=false,
                     UpdateDateTime=DateTime.Now,
                     CreatedDateTime=DateTime.Now,
                      Url= url,
                      Actived =true,
                      Deleted=false,
                      Title=model.CategoryModel.Name,
                      LanguageId = model.CategoryModel.LanguageId
                    });

                    return Redirect(UrlService.AdminGetUrl("category/List"));

                }
                else
                {
                    var url= UrlService.CreateUrl("c", model.CategoryModel.Name, model.CategoryModel.ContentId.ToString());
                    model.CategoryModel.UpdateDateTime = DateTime.Now;
                    model.CategoryModel.CreatedDateTime = DateTime.Now;
                    model.CategoryModel.Url = url;
                    categoryService.Update(model.CategoryModel);
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";

                    var urlModel = urlsServices.GetUrlContentLanguageById(model.CategoryModel.ContentId, model.CategoryModel.LanguageId);
                    urlModel.Type = false;
                    urlModel.UpdateDateTime = DateTime.Now;
                    urlModel.Url = url;
                    urlModel.LanguageId = model.CategoryModel.LanguageId;
                    urlModel.Title = model.CategoryModel.Name;
                    urlsServices.Update(urlModel);
                    return Redirect(UrlService.AdminGetUrl("category/List"));
                }

            }
            return View(model.CategoryModel.Id == 0 ? "Create" : "Edit", model);
        }

       
    }
}