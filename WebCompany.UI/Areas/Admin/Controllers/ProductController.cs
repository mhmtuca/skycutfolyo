﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCompany.Business.AutoMappers;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        private IMetaTagService metaTagService;
        private ICategoryService categoryService;
        private IProductService productService;
        private ISearchKeywordService searchKeywordService;
        private IAdminServices adminServices;
        private readonly ILanguageService languageService;

        public ProductController(
            IMetaTagService metaTagService,
            ICategoryService categoryService,
            IProductService productService,
            ISearchKeywordService searchKeywordService,
            IAdminServices adminServices,
            ILanguageService languageService
            )
        {
            this.metaTagService = metaTagService;
            this.categoryService = categoryService;
            this.productService = productService;
            this.searchKeywordService = searchKeywordService;
            this.adminServices = adminServices;
            this.languageService = languageService;
        }
        public ActionResult Index()
        {
            productService.GetProductList();
            return View();
        }
        public ActionResult Create()
        {
            var model = new ProductAdminViewModel();
            var categoryListModel = categoryService.GetCategoryList();


            model.ContentId = Guid.NewGuid();
            model.LanguageId = 1;
            model.ProductCategoryList = categoryListModel.Select(x => x.ToModel()).ToList();
            return View(model);
        }

        public ActionResult Edit(Guid contentId)
        {
            var model = new ProductViewModel();
            var categoryListModel = categoryService.GetCategoryList();

            var product = productService.GetProductList(contentId);
            model.ProductAdminViewModel = product.Select(f=> f.ToModel()).ToList(); 
            model.LanguageList = languageService.GetLanguageList();
            model.ProductCategoryList = categoryService.GetCategoryList().Select(x => x.ToModel()).ToList();
            return View(model);
        }
        public ActionResult Delete(int Id)
        {
            var product = productService.GetProductId(Id);
            productService.Delete(product);
            TempData["Result"] = true;
            TempData["Message"] = "Kayıt başarılı bir şekilde silinmiştir";
            return Redirect("/admin/product/List");
        }
        public ActionResult List()
        {
            var model = new List<ProductAdminViewModel>();
            if (productService.GetProductList()!=null)
            model = productService.GetProductList().Select(x => x.ToModel()).ToList();

            
            return View(model);
        }

        [ValidateInput(false)]
        public ActionResult Save(ProductAdminViewModel model, IEnumerable<HttpPostedFileBase> Files, FormCollection frm)
        { 

            //(Normal Fiyat - İndirimli Fiyat) / Normal Fiyat) x 100

            var categoryName = categoryService.GetCategoryId(model.CategoryId);

            if (ModelState.IsValid)
            {

                string logoPath = UrlExtensions.ProductNotFound;
                var logoName = new List<string>();
                if (Files != null)
                {
                    foreach (var file in Files)
                    {
                        if (file != null)
                        {
                            var imageConfig = new Config()
                            {
                                FileBase = file,
                                FileName = file.FileName,
                                Width = UrlExtensions.ProductImageWidth,
                                ImagePath = UrlExtensions.ProductImagePath,
                                ContentName = model.Name
                            };
                            logoName.Add(ImageUpload.ImgUploadGaleri(imageConfig));

                        }
                        else
                        {
                            logoName.Add(UrlExtensions.ProductNotFound);
                        }
                    }
                }
                if (model.Id == 0)
                {
                    model.CreatedDateTime = DateTime.Now;
                    model.UpdateDateTime = DateTime.Now;
                    model.Url = UrlService.CreateUrl("p", model.Name, model.ContentId.ToString());
                    model.Image = logoName[0];
                    model.Image2 = logoName[1];
                    model.Image3 = logoName[2];
                    model.Image4 = logoName[3];
                    
                    productService.Insert(model.ToEntity());
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde eklenmiştir";


                }
                else
                {
                    model.UpdateDateTime = DateTime.Now;
                    model.CreatedDateTime = DateTime.Now;
                    model.Url = UrlService.CreateUrl("p", model.Name, model.ContentId.ToString());
                    model.Image = logoName[0] == UrlExtensions.ProductNotFound && model.Image != null ? model.Image : logoName[0];
                    model.Image2 = logoName[1] == UrlExtensions.ProductNotFound && model.Image2 != null ? model.Image2 : logoName[1];
                    model.Image3 = logoName[2] == UrlExtensions.ProductNotFound && model.Image3 != null ? model.Image3 : logoName[2];
                    model.Image4 = logoName[3] == UrlExtensions.ProductNotFound && model.Image4 != null ? model.Image4 : logoName[3];
                    productService.Update(model.ToEntity());
                    ModelState.Clear();
                    TempData["Result"] = true;
                    TempData["Message"] = "Kayıt başarılı bir şekilde güncellenmiştir";

                }
                //SearchKeywords 
                var searchKeywords = new SearchKeywordsDto()
                {
                    ContentId = model.ContentId,
                    Search = $"{model.Name} , {categoryName.Name} , {model.Tag}",
                    ContentTypeId = (int)ContentType.Product,
                    Deleted = false
                };
                //Keywords Ekleme
               // adminServices.AddSearchKeywords(searchKeywords);

                //Meta Tag Ekle

                //adminServices.AddMetaTag(frm, model.ContentId, model.Name, model.Detail, ContentType.Product);
                return Redirect(UrlService.AdminGetUrl("product/List"));
            }

            var categoryListModel = categoryService.GetCategoryList();
            model.ProductCategoryList = categoryListModel.Select(x=> x.ToModel()).ToList();
            return View(model.Id == 0 ? "Create" : "Edit", model);
        }

    }
}