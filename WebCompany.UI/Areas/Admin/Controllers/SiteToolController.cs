﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Controllers
{
    public class SiteToolController : Controller
    {
        private ISettingService SettingService;
        private IMetaTagService metaTagService;
        private IAdminServices adminServices;
        private ILanguageService languageService;


        public SiteToolController(ISettingService SettingService, IMetaTagService metaTagService, IAdminServices adminServices, ILanguageService languageService)
        {
            this.SettingService = SettingService;
            this.metaTagService = metaTagService;
            this.adminServices = adminServices;
            this.languageService = languageService;
        }
        public ActionResult Index()
        {

            var language = languageService.GetLanguageList();
            var model = SettingService.GetSettingListId();

            var siteModel = new SiteModel();
            siteModel.LanguageListModel = language;
            siteModel.SettingListModel = SettingService.GetSettingList();
            // var MetaTag = metaTagService.GetMetaTagContentId(model.ContentId);
            // siteModel.MetaTagModel = MetaTag;
            return View(siteModel);
        }

        [ValidateInput(false)]
        public ActionResult Save(
        SiteModel model,
        IEnumerable<HttpPostedFileBase> Images,
        FormCollection frm)
        {
            Guid ContentId = Guid.NewGuid();
            if (ModelState.IsValid)
            {

                string logoPath = null;
                string Icon = null;
                string HomeContactPath = null;

                int a = 1;
                foreach (var image in Images)
                {

                    if (image != null && a == 1)
                        logoPath = ImageUpload.ImgUploadGaleri(new Config() { FileBase = image, FileName = image.FileName, ContentName = "Logo", ImagePath = UrlExtensions.ImagePath, Width = UrlExtensions.ImagePathWidth });

                    if (image != null && a == 2)
                        Icon = ImageUpload.ImgUploadGaleri(new Config() { FileBase = image, FileName = image.FileName, ContentName = "favicon", ImagePath = UrlExtensions.ImagePath, Width = "32,32,32" });

                    if (image != null && a == 3)
                        HomeContactPath = ImageUpload.ImgUploadGaleri(new Config() { FileBase = image, FileName = image.FileName, ContentName = "ContactImage", ImagePath = UrlExtensions.ImagePath, Width = "1110,500,352" });
                    a++;
                }

                // adminServices.AddMetaTag(frm, model.SettingModel.ContentId!=null? model.SettingModel.ContentId: ContentId, model.SettingModel.Title, model.SettingModel.Title, ContentType.CompanySite);
                if (model.SettingModel.Id == 0)
                {

                    model.SettingModel.Actived = true;
                    model.SettingModel.Deleted = false;
                    model.SettingModel.CreatedDateTime = DateTime.Now;
                    model.SettingModel.UpdateDateTime = DateTime.Now;
                    model.SettingModel.Logo = logoPath;
                    model.SettingModel.Icon = Icon;
                    model.SettingModel.HomeContactPath = HomeContactPath;
                    SettingService.Insert(model.SettingModel);
                    return Redirect("/admin/sitetool/Index");
                }
                else
                {
                    model.SettingModel.Actived = true;
                    model.SettingModel.Deleted = false;
                    model.SettingModel.CreatedDateTime = DateTime.Now;
                    model.SettingModel.UpdateDateTime = DateTime.Now;
                    model.SettingModel.Logo = logoPath != null ? logoPath : model.SettingModel.Logo;
                    model.SettingModel.Icon = Icon != null ? Icon : model.SettingModel.Icon;
                    model.SettingModel.HomeContactPath = HomeContactPath != null ? HomeContactPath : model.SettingModel.HomeContactPath;
                    SettingService.Update(model.SettingModel);
                    return Redirect("/admin/sitetool/Index");
                }


            }
            return View("Index", model);
        }
    }
}