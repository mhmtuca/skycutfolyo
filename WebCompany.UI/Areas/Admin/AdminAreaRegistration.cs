﻿using System.Web.Mvc;

namespace WebCompany.UI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
              "message",
              "Admin/Message/List/{page}",
              new { Controller= "Message", action = "List", page = UrlParameter.Optional }
               , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
          );

            context.MapRoute(
                "Admin_default_Product_Edit",
                "Admin/Product/edit/{contentId}",
                new { Controller = "Product", action = "Edit", contentId = UrlParameter.Optional }
                  , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
            );


            context.MapRoute(
              "Admin_default_Page_Edit",
              "Admin/Page/edit/{contentId}",
              new { Controller = "Page", action = "Edit", contentId = UrlParameter.Optional }
                , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
          );
            context.MapRoute(
             "Admin_default_video_Edit",
             "Admin/video/edit/{contentId}",
             new { Controller = "Video", action = "Edit", contentId = UrlParameter.Optional }
               , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
         );

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
                  , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
            );

            
            context.MapRoute(
              "kategori",
              "Admin/kategori/{action}/{id}",
              new { action = "Index", id = UrlParameter.Optional }
               , namespaces: new string[] { "WebCompany.UI.Areas.Admin.Controllers" }
          );
        }
    }
}