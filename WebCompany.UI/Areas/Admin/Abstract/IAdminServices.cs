﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Data;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Abstract
{
    public interface IAdminServices
    {
         List<CategoryViewModel> GetCategoryView(List<CategoryDto> Category);
        //void AddMetaTag(FormCollection frm, Guid ContentId, string Title, string Detail, ContentType contentType);
        //void AddSearchKeywords(SearchKeywordsDto search);
    }
}