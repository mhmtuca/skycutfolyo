﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebCompany.Business.Models;
using WebCompany.Core;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Models;

namespace WebCompany.UI.Areas.Admin.Abstract
{
    public class AdminServices : IAdminServices
    {
        private IMetaTagService metaTagService;
        private ISearchKeywordService searchKeywordService;
        public AdminServices(
        IMetaTagService metaTagService,
        ISearchKeywordService searchKeywordService)
        {
            this.metaTagService = metaTagService;
            this.searchKeywordService = searchKeywordService;
        }
        //public void AddMetaTag(FormCollection frm, Guid ContentId,string Title,string Detail, ContentType contentType)
        //{

        //    var metaTag = new MetaTagDto()
        //    {
        //        Actived = true,
        //        ContentId = ContentId,
        //        MetaTitle = !string.IsNullOrEmpty(frm["MetaTitle"]) ? frm["MetaTitle"] : $"{Title} | {UrlExtensions.SiteName}",
        //        MetaDescription = !string.IsNullOrEmpty(frm["MetaDescription"]) ? frm["MetaDescription"] : $"{ (Detail.Length > 180 ? Detail.Substring(0, 180) : Detail)}",
        //        MetaKeywords = !string.IsNullOrEmpty(frm["MetaKeywords"]) ? frm["MetaKeywords"] : "",
        //        CreatedDateTime = DateTime.Now,
        //        UpdateDateTime = DateTime.Now,
        //        Deleted = false,
        //        Type = (int)contentType
        //    };

        //    var GetmetaTag = metaTagService.GetMetaTagContentId(ContentId);
        //    if (GetmetaTag==null)
        //    {
        //        metaTagService.Insert(metaTag);
        //    }
        //    else
        //    {
        //        GetmetaTag.MetaTitle = metaTag.MetaTitle;
        //        GetmetaTag.MetaKeywords = metaTag.MetaKeywords;
        //        GetmetaTag.MetaDescription = metaTag.MetaDescription;
        //        GetmetaTag.Type = (int)contentType;
        //        metaTagService.Update(GetmetaTag);
        //    }
        //}

        //public void AddSearchKeywords(SearchKeywordsDto search)
        //{
        //    var GetSearch = searchKeywordService.GetSearchKeywordContentId(search.ContentId);
        //    if (GetSearch != null)
        //    {
        //        GetSearch.Search = search.Search;
        //        GetSearch.ContentTypeId = (int)search.ContentTypeId;
        //        searchKeywordService.Update(GetSearch);
        //    }
        //    else
        //    {
        //        searchKeywordService.Insert(search);
        //    }

        //}

        public List<CategoryViewModel> GetCategoryView(List<CategoryDto> Category)
        {
            return Category.Select(x => CreateGetCategoryView(x)).ToList();

        }

        private CategoryViewModel CreateGetCategoryView(CategoryDto category)
        {
            return new CategoryViewModel()
            {
                Actived = category.Actived,
                CompanyId = category.CompanyId,
                CreatedDateTime = category.CreatedDateTime,
                Deleted = category.Deleted,
                Detail = category.Detail,
                Id = category.Id,
                Name = category.Name,
                UpdateDateTime = category.UpdateDateTime,
                ContentId = category.ContentId,
                LanguageId = category.LanguageId,
                Url = category.Url
            };
        }

    }
}