$(function() {
	/*
		//Mainpage --> Main carousel init
	*/
	$(".carousel").carousel({
		swipe: 30 // percent-per-second, default is 50. Pass false to disable swipe
	});
	
	/*
		//Navigation --> Add dropdown effect
	*/
	$('.navbar .dropdown').hover(function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideToggle(400);
    }, function() {
		$(this).find('.dropdown-menu').first().stop(true, true).slideToggle(400);
    });
	
	/*
		//lightbox
	*/
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});
	
	/*
		//product-detail thumbnail
	*/
	$('.js-product-detail-thumbnail-link').click(function(){
		var imgSrc = $(this).attr('data-img');
		var imgSrcZoom = $(this).attr('data-img-zoom');
		$('.product-detail__img').attr({'src':imgSrc});
		$('.product-detail__img-link').attr({'href':imgSrcZoom}).hide().fadeIn(250);
	});
	
	/*
		//click version
	*/
	//Add slideDown animation to Bootstrap dropdown when expanding.
	// $('.dropdown').on('show.bs.dropdown', function() {
		// $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
	// });
	//Add slideUp animation to Bootstrap dropdown when collapsing.
	// $('.dropdown').on('hide.bs.dropdown', function() {
		// $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	// });
	
 });
