﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCompany.UI.Infrastructure
{
    public class Duration
    {
        private static Duration instance;

        private Duration() { }

        public static Duration Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Duration();
                }
                return instance;
            }
        }

        private int durationValue;
        public int DurationValue { get
            {
                return  Convert.ToInt32(WebCompany.Core.UrlExtensions.SiteDuration);
            }
            set
            {
                durationValue = value;
            }
        }
    }
}