﻿using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Unity;
using WebCompany.Business.Logic;
using WebCompany.Data;
using WebCompany.Services;
using WebCompany.UI.Areas.Admin.Abstract;

namespace WebCompany.UI.Infrastructure
{
    public static class BootStrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {


            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers 
            container.RegisterType<IDalContext, DalContext>();
            container.RegisterType<ISliderService, SliderService>();
            container.RegisterType<ISearchKeywordService, SearchKeywordService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<IMetaTagService, MetaTagService>(); 
            container.RegisterType<ISettingService, SettingService>();
            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<IPageService, PageService>();
            container.RegisterType<IPageContentService, PageContentService>();
            container.RegisterType<IAdminServices, AdminServices>();
            container.RegisterType<IMenuService, MenuService>(); 
            container.RegisterType<IGalleryService, GalleryService>();
            container.RegisterType<IBlogService, BlogService>();
            container.RegisterType<IMessageService, MessageService>();
            container.RegisterType<IUrlsService, UrlsService>();
            container.RegisterType<IGalleryImageService, GalleryImageService>();
            container.RegisterType<ILanguageService, LanguageService>();
            container.RegisterType<IVideoService, VideoService>();
            container.RegisterType<IDocumentService, DocumentService>();
            container.RegisterType<IDocumentLogic, DocumentLogic>();
            container.RegisterType<IHomeLogic, HomeLogic>();
            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }

    }
}
