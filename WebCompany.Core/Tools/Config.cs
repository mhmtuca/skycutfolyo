﻿using System.Web;

namespace WebCompany.Core
{
    public class Config
    {
        public HttpPostedFileBase FileBase { get; set; }
        public string Width { get; set; }
        public string ImagePath { get; set; }
        public string FileName { get; set; } 
        public string ContentName { get; set; }

    }
}
