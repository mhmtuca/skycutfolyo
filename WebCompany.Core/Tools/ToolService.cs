﻿namespace WebCompany.Core
{
    public static class ToolService
    {
        public static string WordCleaning(string words)
        {
            words = words.Replace("-", "");
            words = words.Replace(" ", "-");
            words = words.Replace("ç", "c");
            words = words.Replace("ğ", "g");
            words = words.Replace("ı", "i");
            words = words.Replace("ö", "o");
            words = words.Replace("ş", "s");
            words = words.Replace("ü", "u");
            words = words.Replace("Ç", "C");
            words = words.Replace("Ğ", "G");
            words = words.Replace("İ", "i");
            words = words.Replace("I", "i");
            words = words.Replace("Ö", "O");
            words = words.Replace("Ş", "S");
            words = words.Replace("Ü", "U");
            words = words.Replace("\"", "");
            words = words.Replace("/", "");
            words = words.Replace("(", "");
            words = words.Replace(")", "");
            words = words.Replace("{", "");
            words = words.Replace("}", "");
            words = words.Replace("%", "");
            words = words.Replace("&", "");
            words = words.Replace("+", "");
            words = words.Replace(".", "");
            words = words.Replace("?", "");
            words = words.Replace(",", "");
            words = words.Replace(":", "");
            words = words.Replace(".", "");
            words = words.Replace("~", "");
            words = words.Replace("´", "");
            words = words.Replace("'", "");
            words = words.Replace("\"", "");
            words = words.Replace("é", "");
            words = words.Replace(",", "");
            words = words.Replace("*", "");
            words = words.Replace("æ", "");
            words = words.Replace("#", "");
            words = words.Replace("!", "");
            words = words.Replace("’", "");
            words = words.Replace(";", "");
            words = words.Replace("---", "-");
            words = words.Replace("--", "-");
            words = words.TrimEnd('-');

            return words.ToLower();
        }
    }
}
