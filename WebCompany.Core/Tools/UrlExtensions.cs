﻿using System;
using System.Web.Configuration;

namespace WebCompany.Core
{
    public class UrlExtensions
    {
        public static string SiteUrl { get { return WebConfigurationManager.AppSettings["SiteUrl"]; } }
        //public static int SiteId { get { return Convert.ToInt32(WebConfigurationManager.AppSettings["SiteId"]!=null? WebConfigurationManager.AppSettings["SiteId"]:"0"); } }
        public static Guid SiteContentId { get { return Guid.Parse(WebConfigurationManager.AppSettings["SiteContentId"] != null ? WebConfigurationManager.AppSettings["SiteContentId"].ToString() : Guid.NewGuid().ToString()); } }
        public static string SiteName { get { return WebConfigurationManager.AppSettings["SiteName"]; } }

        //Todo: Product Path Setting
        public static string ProductImagePath { get { return WebConfigurationManager.AppSettings["ProductImagePath"]; } }
        public static string ProductImageWidth { get { return  WebConfigurationManager.AppSettings["ProductImageWidth"]; }   }
        public static string ProductNotFound { get { return WebConfigurationManager.AppSettings["ProductNotFound"]; } }

        //Todo: Page Path Setting
        public static string PageImagePath { get { return WebConfigurationManager.AppSettings["PageImagePath"]; } }
        public static string PageImageWidth { get { return WebConfigurationManager.AppSettings["PageImageWidth"]; } }
        public static string PageNotFound { get { return WebConfigurationManager.AppSettings["PageNotFound"]; } }

        //Todo: Slider Path Setting
        public static string SliderImagePath { get { return WebConfigurationManager.AppSettings["SliderImagePath"]; } }
        public static string SliderImageWidth { get { return WebConfigurationManager.AppSettings["SliderImageWidth"]; } }
        public static string SliderNotFound { get { return WebConfigurationManager.AppSettings["SliderNotFound"]; } }

        //Todo: Site Path Setting
        public static string ImagePath { get { return WebConfigurationManager.AppSettings["ImagePath"]; } }
        public static string ImagePathWidth { get { return  WebConfigurationManager.AppSettings["ImagePathWidth"]; } }
        public static string ImagePathNotFound { get { return WebConfigurationManager.AppSettings["ImagePathNotFound"]; } }

        //Todo: Gallery Path Setting
        public static string GalleryPath { get { return WebConfigurationManager.AppSettings["GalleryPath"]; } }
        public static string GalleryWidth { get { return WebConfigurationManager.AppSettings["GalleryWidth"]; } }
        public static string GalleryNotFound { get { return WebConfigurationManager.AppSettings["GalleryNotFound"]; } }

        //Todo: Blog Path Setting
        public static string BlogPath { get { return WebConfigurationManager.AppSettings["BlogPath"]; } }
        public static string BlogWidth { get { return WebConfigurationManager.AppSettings["BlogWidth"]; } }
        public static string BlogNotFound { get { return WebConfigurationManager.AppSettings["BlogNotFound"]; } }

        //Todo: Document Path Setting
        public static string DocumentPath { get { return WebConfigurationManager.AppSettings["DocumentPath"]; } }
 
        //Todo: Video Path Setting
        public static string VideoPath { get { return WebConfigurationManager.AppSettings["VideoPath"]; } }
        public static string VideoWidth { get { return WebConfigurationManager.AppSettings["VideoWidth"]; } }
        public static string VideoNotFound { get { return WebConfigurationManager.AppSettings["VideoNotFound"]; } }
        //PageSize
        public static int PageSize { get { return Convert.ToInt16(WebConfigurationManager.AppSettings["PageSize"]); } }

        //Duration
        public static int SiteDuration { get { return Convert.ToInt32(WebConfigurationManager.AppSettings["SiteDuration"]); } }

        //View Image Path
        public static string GalleryViewPath { get { return WebConfigurationManager.AppSettings["GalleryViewPath"]; } }
        public static string ProductViewPath { get { return WebConfigurationManager.AppSettings["ProductViewPath"]; } }
        public static string BlogViewPath { get { return WebConfigurationManager.AppSettings["BlogViewPath"]; } }
        public static string PageViewPath { get { return WebConfigurationManager.AppSettings["PageViewPath"]; } } 
        public static string SliderViewPath { get { return WebConfigurationManager.AppSettings["SliderViewPath"]; } }
        public static string ImageViewPath { get { return WebConfigurationManager.AppSettings["ImageViewPath"]; } }
        public static string VideoViewPath { get { return WebConfigurationManager.AppSettings["VideoViewPath"]; } }

        //Login
        public static string Email { get { return WebConfigurationManager.AppSettings["Email"]; } } 
        public static string Password { get { return WebConfigurationManager.AppSettings["Password"]; } } 
    }
}
