﻿using System;

namespace WebCompany.Core
{
    public static class UrlService
    {
        public static string SiteUrl = WebCompany.Core.UrlExtensions.SiteUrl; 
        public static string AdminGetUrl(string url)
        {
            return $"{SiteUrl}/admin/{url}";
        }

        public static string CreateUrl(string page,string title,string Id)
        {
            return $"{page}/{ToolService.WordCleaning(title)}/{Id}";
        }

        public static string MenuCreateUrl(int menuTypeId,string title, string Id)
        {
            if (menuTypeId==1)
            {
             return $"sayfa/{ToolService.WordCleaning(title)}/{Id}";
            }
            else if (menuTypeId == 2)
            {
                return $"kategori/{ToolService.WordCleaning(title)}/{Id}";
            }
            else
            {
                return "";
            }

        }

        public static string ImagePathHomeView(string path,string type, string Image)
        {
          return $"{SiteUrl}/{path}/{type}/{Image}";  
        }
        public static string DocumentPathHomeView(string path)
        {
            return $"{SiteUrl}/content/document/{path}";
        }
        public static string WebUrl(string title,string kategori, Guid ContentId)
        {
            return $"{SiteUrl}/{kategori}/{ToolService.WordCleaning(title)}/{ContentId}";
        }
        public static string WebViewUrl(string url)
        {
            return $"{SiteUrl}/{url}";
        }
    }
}
