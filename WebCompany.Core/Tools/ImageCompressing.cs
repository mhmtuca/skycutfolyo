﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace WebCompany.Core.Tools
{
    public class ImageCompressing
    {

        public void GetImages(string path, string fileName)
        {
            string img = $"{path}\\{fileName}";
            byte[] b = File.ReadAllBytes(img);

            // Write compressed data with DeflateStream.
            string outputPath = $"{path}\\temp.png";
            using (FileStream f2 = new FileStream(outputPath, FileMode.Create))
            using (DeflateStream deflate = new DeflateStream(f2,
                CompressionMode.Compress, false))
            {
                deflate.Write(b, 0, b.Length);
            }

            string inputStr = "Hello world!";
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputStr);

            using (var outputStream = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(outputStream, CompressionMode.Compress))
                    gZipStream.Write(inputBytes, 0, inputBytes.Length);

                var outputBytes = outputStream.ToArray();

                var outputbase64 = Convert.ToBase64String(outputBytes);
                using (FileStream f2 = new FileStream(outputPath, FileMode.Create))
                using (DeflateStream deflate = new DeflateStream(f2,
                    CompressionMode.Compress, false))
                {
                    deflate.Write(b, 0, b.Length);
                }
            }
            //FileInfo info1 = new FileInfo(img);
            //if (info1.Exists)
            //{
            //    info1.Delete();
            //}
            //System.IO.File.Move(imgTemp, img);
        }

        public List<ImageView> ListImages()
        {

            var imagesList = new List<ImageView>();
            string[] main = new string[] {"", "b", "g", "p", "u", "s", "desktop", "mobile", "tablet" };
            string[] altMain = new string[] { "","desktop", "mobile", "tablet" };
            foreach (var item in main)
            {
                foreach (var alt in altMain)
                {
                    DirectoryInfo dir = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath($"/content/img/{item}/{alt}"));
                    if (dir.Exists)
                    {

                        FileInfo[] file = dir.GetFiles();

                        foreach (FileInfo file2 in file)
                        {
                            if (file2.Extension == ".jpg" || file2.Extension == ".jpeg" || file2.Extension == ".gif" || file2.Extension == ".png")
                            {
                                imagesList.Add(new ImageView() { FullName = file2.Name, Path = file2.DirectoryName });
                            }
                        }


                    }
                }
            }
            return imagesList;
        }
        public class ImageView
        {
            public string Path { get; set; }
            public string FullName { get; set; }
        }


        public void CompressImage(string path, string fileName, int imageQuality)
        {
            string img = $"{path}\\{fileName}";
            Image sourceImage = Image.FromFile(img);
            string outputPath = $"{path}\\temp.png";
            try
            {
                //Create an ImageCodecInfo-object for the codec information
                ImageCodecInfo jpegCodec = null;

                //Set quality factor for compression
                EncoderParameter imageQualitysParameter = new EncoderParameter(
                            System.Drawing.Imaging.Encoder.Quality, imageQuality);

                //List all avaible codecs (system wide)
                ImageCodecInfo[] alleCodecs = ImageCodecInfo.GetImageEncoders();

                EncoderParameters codecParameter = new EncoderParameters(1);
                codecParameter.Param[0] = imageQualitysParameter;

                //Find and choose JPEG codec
                for (int i = 0; i < alleCodecs.Length; i++)
                {
                    if (alleCodecs[i].MimeType == "image/jpeg")
                    {
                        jpegCodec = alleCodecs[i];
                        break;
                    }
                    else if (alleCodecs[i].MimeType == "image/png")
                    {
                        jpegCodec = alleCodecs[i];
                        break;
                    }
                }

                //Save compressed image
                sourceImage.Save(outputPath, jpegCodec, codecParameter);
                sourceImage.Dispose();
                FileInfo info1 = new FileInfo(img);
                if (info1.Exists)
                {
                    info1.Delete();
                }
                System.IO.File.Move(outputPath, img);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GetImageComp(string filename)
        {
            using (var outStream = new MemoryStream())
            {
                using (var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    using (var gzipStream = new GZipStream(outStream, CompressionMode.Compress))
                    {
                        fileStream.CopyTo(gzipStream);
                    }
                    return "";
              //     return string.Format("Compressed from {0} to {1} bytes", fileStream.Length, outStream.Length);

                    // "outStream" is utilised here (persisted to a NoSql database).
                }
            }
        }
        public void VaryQualityLevel(string path,string fileName)
        {
          
            string FullPath = $"{path}//{fileName}";
            var sourceImage = Image.FromFile(FullPath,true);
            Image myImage = Image.FromFile(FullPath, true);
            string FullPath2 = $"{path}//temp.jpeg";
            // Get a bitmap. The using statement ensures objects  
            // are automatically disposed from memory after use.  
            try
            {
                EncoderParameters encoder_params = new EncoderParameters(1);
                encoder_params.Param[0] = new EncoderParameter(
                    System.Drawing.Imaging.Encoder.Quality, 0);

                ImageCodecInfo image_codec_info =
                    GetEncoder(ImageFormat.Jpeg);
                //File.Delete(FullPath);
                myImage.Save(FullPath2, image_codec_info, encoder_params);


              
                //FileInfo info1 = new FileInfo(FullPath);
                //if (info1.Exists)
                //{
                //    info1.Delete();
                //}
                //System.IO.File.Move(FullPath2, FullPath);

           
            }
            catch (Exception e)
            {
                throw e;
            }
          
        }
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        public  void SaveJpeg(Image img,string path,string fileName, int quality)
        {
            if (quality < 0 || quality > 100)
                throw new ArgumentOutOfRangeException("quality must be between 0 and 100.");
            string FullPath = $"{path}//{fileName}";
            string FullPath2 = $"{path}//temp.{FullPath.Split('.').LastOrDefault()}";
            // Encoder parameter for image quality 
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            // JPEG image codec 
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/"+FullPath.Split('.').LastOrDefault().Replace("jpg","jpeg"));
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(FullPath2, jpegCodec, encoderParams);
            img.Dispose();
            FileInfo info1 = new FileInfo(FullPath);
            if (info1.Exists)
            {
                info1.Delete();
            }
            System.IO.File.Move(FullPath2, FullPath);
        }

        /// <summary> 
        /// Returns the image codec with the given mime type 
        /// </summary> 
        private  ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];

            return null;
        }
    }
}
