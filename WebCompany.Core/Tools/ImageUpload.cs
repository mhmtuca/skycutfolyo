﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Configuration;
using WebCompany.Core.Tools;

namespace WebCompany.Core
{
    public class ImageUpload
    {
        static string path = WebConfigurationManager.AppSettings["ImgPath"];
        public static string ImgUploadGaleri(Config imageConfig)
        {

            string fileName = ToolService.WordCleaning(imageConfig.ContentName) + "-" + System.Guid.NewGuid().ToString().Replace("-", "").ToString().Substring(0, 4) + ".png";
            string orj = $"{imageConfig.ImagePath}\\orjinal\\{imageConfig.FileName}";
            imageConfig.FileBase.SaveAs(orj);

            //Kayıt ettğimiz fotoyu çağırıp üzerinde işlem yapıyoruz
            int i = 1;
            foreach (var item in imageConfig.Width.Split(','))
            {
                string path = "";
                switch (i)
                {
                    case 1:
                        path = imageConfig.ImagePath + "\\mobile";
                        break;
                    case 2:
                        path = imageConfig.ImagePath + "\\tablet";
                        break;
                    case 3:
                        path = imageConfig.ImagePath + "\\desktop";
                        break;
                    default:
                        path = imageConfig.ImagePath + "\\desktop";
                        break;
                }

                AddImageUpload(imageConfig.FileBase, orj, path, fileName, item);

                var myImage = System.Drawing.Image.FromFile(path + "\\" + fileName, true);
                new ImageCompressing().SaveJpeg(myImage, path, fileName, 0);
                i++;
            }
            return fileName;
        }
        public static void fullImageOptimize()
        {
            var imageList = new ImageCompressing().ListImages();
            // new ImageCompressing().GetImageComp(imageList.().Path+"\\"+imageList.FirstOrDefault().FullName);

            foreach (var item in imageList)
            {
                var myImage = System.Drawing.Image.FromFile(item.Path + "\\" + item.FullName, true);
                new ImageCompressing().SaveJpeg(myImage, item.Path, item.FullName, 0);
            }
        }
        static string documentPath = WebConfigurationManager.AppSettings["DocumentPath"];

        public class FileUploadModel
        {
            public bool IsError { get; set; }
            public string FileName { get; set; }
        }
        public static FileUploadModel FileUpload(HttpPostedFileBase Yukle)
        {
            var model = new FileUploadModel();
            string fileName = System.Guid.NewGuid().ToString().Replace("-", "").ToString().Substring(0, 10) + Path.GetExtension(Yukle.FileName);
            if (Yukle != null)
            {
                try
                {

                    if (Yukle.ContentLength < 102400)
                    {
                        var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                        if (!supportedTypes.ToString().Contains(Yukle.ContentType))
                        {
                            Yukle.SaveAs((documentPath +"/"+ fileName));
                        }
                        else
                        {
                            model.IsError = true;
                            model.FileName = "Upload status: Only JPEG files are accepted!";
                        }
                    }
                    else
                    {
                        model.IsError = true;
                        model.FileName = "Upload status: The file has to be less than 100 kb!";
                    }
                  


                }
                catch (Exception ex)
                {
                    model.IsError = true;
                    model.FileName = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }



            model.FileName = fileName;


            return model;
        }
        public static void AddImageUpload(HttpPostedFileBase file, string orj, string path, string fileName, string width)
        {
            using (Bitmap OriginalBMb = new Bitmap(orj))
            {
                double ResBoy = OriginalBMb.Height;
                double ResEn = OriginalBMb.Width;
                double oran = 0;
                if (ResEn >= Convert.ToInt16(width))
                {
                    oran = ResEn / ResBoy;
                    ResEn = Convert.ToDouble(width);
                    ResBoy = Convert.ToDouble(width) / oran;

                    Size newSizeb = new Size(Convert.ToInt32(ResEn), Convert.ToInt32(ResBoy));
                    Bitmap Resizebmb = new Bitmap(OriginalBMb, newSizeb);
                    Graphics grPhoto = Graphics.FromImage(Resizebmb);
                    grPhoto.InterpolationMode = InterpolationMode.High;

                    Resizebmb.Save(path + "\\" + fileName, ImageFormat.Png);
                    OriginalBMb.Dispose();
                }
                else
                {
                    file.SaveAs(path + "\\" + fileName);
                }
            }
        }
    }
}
