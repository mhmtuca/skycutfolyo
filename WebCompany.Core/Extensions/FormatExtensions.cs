﻿using System;
using System.Globalization;

namespace WebCompany.Core.Extensions
{
    public static class FormatExtensions
    {
        public static string FormatDecimal(this decimal value)
        {
            return string.Format("{0:0,0.00}", value); 
        }

        public static decimal ConvertDecimal(this decimal value)
        {

            return decimal.Round(value, 2, MidpointRounding.AwayFromZero);  
        }


        public static string CurrencyUnit(this int Id)
        {
            switch (Id)
            {
                case 1:
                    return "TL";
                case 2:
                    return "$";
                case 3:
                    return "€";
                default:
                    return "TL";
            }
          
        }
    }
}
