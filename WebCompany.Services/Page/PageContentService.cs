﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class PageContentService : IPageContentService
    {
        private IDalContext context;

        public PageContentService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<PageContentDto> GetPageContentList()
        {
            return context.PageContentsData.All().Where(x => x.CompanyId == companyaId && x.Deleted == false).OrderByDescending(x => x.Id).ToList();
        }

        public PageContentDto GetPageContentById(int pageId)
        {
            return context.PageContentsData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Deleted == false && x.Id==pageId);
        }
        public PageContentDto GetPageContentContentId(Guid ContentId)
        {
            return context.PageContentsData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Deleted == false && x.ContentId == ContentId);
        }
        public void Insert(PageContentDto page)
        {
            page.CompanyId = companyaId;
            context.PageContentsData.Create(page);
        }

        public void Update(PageContentDto page)
        {
            page.CompanyId = companyaId;
            context.PageContentsData.Update(page);
        }

        public void Delete(PageContentDto page)
        {
            page.CompanyId = companyaId;
            context.PageContentsData.Delete(page);
        } 

    
    }
}
