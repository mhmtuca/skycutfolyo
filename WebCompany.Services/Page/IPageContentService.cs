﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IPageContentService
    {
        List<PageContentDto> GetPageContentList();
        PageContentDto GetPageContentById(int Id);
        PageContentDto GetPageContentContentId(Guid ContentId); 
        void Insert(PageContentDto PageContent);
        void Update(PageContentDto PageContent);
        void Delete(PageContentDto PageContent);
    }
}
