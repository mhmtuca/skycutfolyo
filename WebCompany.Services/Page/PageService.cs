﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class PageService : IPageService
    {
        private IDalContext context;

        public PageService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<PagesDto> GetPageList()
        {
            return context.PagesData.All().Where(x => x.CompanyId == companyaId && x.Deleted == false).OrderByDescending(x => x.Id).ToList();
        }
        public List<PagesDto> GetPageListByContentId(Guid contentId)
        {
            return context.PagesData.All().Where(x => x.ContentId==contentId &&  x.CompanyId == companyaId && x.Deleted == false).OrderByDescending(x => x.Id).ToList();
        }
        public PagesDto GetPageId(int pageId)
        {
            return context.PagesData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Deleted == false && x.Id==pageId);
        }
        public PagesDto GetPageContentId(Guid ContentId)
        {
            return context.PagesData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Deleted == false && x.ContentId == ContentId);
        }
        public void Insert(PagesDto page)
        {
            page.CompanyId = companyaId;
            context.PagesData.Create(page);
        }

        public void Update(PagesDto page)
        {
            page.CompanyId = companyaId;
            context.PagesData.Update(page);
        }

        public void Delete(PagesDto page)
        {
            page.CompanyId = companyaId;
            context.PagesData.Delete(page);
        }

       
    }
}
