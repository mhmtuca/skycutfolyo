﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IPageService
    {
        List<PagesDto> GetPageList();

        List<PagesDto> GetPageListByContentId(Guid contentId);
        PagesDto GetPageId(int pageId); 
        PagesDto GetPageContentId(Guid ContentId); 
        void Insert(PagesDto page);
        void Update(PagesDto page);
        void Delete(PagesDto page);
    }
}
