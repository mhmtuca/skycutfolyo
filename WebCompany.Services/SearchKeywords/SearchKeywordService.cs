﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class SearchKeywordService : ISearchKeywordService
    {
        private IDalContext context;

        public SearchKeywordService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<SearchKeywordsDto> GetSearchKeywordList()
        {
            return context.SearchKeywordsData.All().Where(x=> x.CompanyId== companyaId && x.Deleted == false).ToList();
        }

        public SearchKeywordsDto GetSearchKeywordId(Guid keywordId)
        {
            return context.SearchKeywordsData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.ContentId== keywordId);
        }

        public SearchKeywordsDto GetSearchKeywordContentId(Guid contentId)
        {
            return context.SearchKeywordsData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.ContentId == contentId &&  x.Deleted == false);
        }

        public void Insert(SearchKeywordsDto searchKeywordDto)
        {
            searchKeywordDto.CompanyId = companyaId;
            context.SearchKeywordsData.Create(searchKeywordDto);
        }

        public void Update(SearchKeywordsDto searchKeywordDto)
        {
            searchKeywordDto.CompanyId = companyaId;
            context.SearchKeywordsData.Update(searchKeywordDto);
        }

        public void Delete(SearchKeywordsDto searchKeywordDto)
        {
            searchKeywordDto.CompanyId = companyaId;
            context.SearchKeywordsData.Delete(searchKeywordDto);
        }
    }
}
