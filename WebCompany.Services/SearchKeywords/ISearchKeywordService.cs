﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ISearchKeywordService
    {
        List<SearchKeywordsDto> GetSearchKeywordList();
        SearchKeywordsDto GetSearchKeywordId(Guid keywordId);
        SearchKeywordsDto GetSearchKeywordContentId(Guid ContentId);
        void Insert(SearchKeywordsDto searchKeywordDto);
        void Update(SearchKeywordsDto searchKeywordDto);
        void Delete(SearchKeywordsDto searchKeywordDto);
    }
}
