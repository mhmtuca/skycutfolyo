﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class SettingService : ISettingService
    {
        private IDalContext context;

        public SettingService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
         
        public List<SettingDto> GetSettingList()
        {
            return context.SettingData.All().Where(x=> x.ContentId == companyaId && x.Deleted==false).ToList();
        }
        public SettingDto GetLastFirmId()
        {
            return context.SettingData.All().Where(x => x.Deleted == false && x.ContentId == companyaId).OrderByDescending(x=> x.Id).FirstOrDefault();
        }
        public SettingDto GetSettingListId()
        {
            return context.SettingData.All().FirstOrDefault(x => x.ContentId == companyaId && x.Deleted==false);
        }

        public void Insert(SettingDto Setting)
        {
            context.SettingData.Create(Setting);
        }

        public void Update(SettingDto Setting)
        {
            context.SettingData.Update(Setting);
        }

        public void Delete(SettingDto Setting)
        {
            context.SettingData.Delete(Setting);
        }
    }
}
