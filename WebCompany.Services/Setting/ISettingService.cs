﻿using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ISettingService
    {
        List<SettingDto> GetSettingList();  
        SettingDto GetSettingListId();
        SettingDto GetLastFirmId();
        void Insert(SettingDto Setting);
        void Update(SettingDto Setting);
        void Delete(SettingDto Setting);
    }
}
