﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IDocumentService
    {
        List<DocumentDto> GetDocumentList();
        List<DocumentDto> GetDocumentListByContentId(Guid contentId);
        DocumentDto GetDocumentById(int DocumentId);
        DocumentDto GetDocumentContentById(Guid contentId);
        DocumentDto GetDocumentContentLanguageById(Guid contentId,int LanguageId);
        void Insert(DocumentDto Document);
        void Update(DocumentDto Document);
        void Delete(DocumentDto Document);
    }
}
