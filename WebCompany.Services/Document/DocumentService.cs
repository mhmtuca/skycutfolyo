﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class DocumentService : IDocumentService
    {
        private IDalContext context;

        public DocumentService(IDalContext dal)
        {
            context = dal;
        }
 
        //private static int companyaId = UrlExtensions.SiteId;
        private static Guid CompanyContentId = UrlExtensions.SiteContentId;

        public List<DocumentDto> GetDocumentList()
        {
            return context.DocumentData.All().Where(x=> x.CompanyId== CompanyContentId && !x.Deleted).ToList();
        }

        public DocumentDto GetDocumentById(int DocumentId)
        {
            return context.DocumentData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.Id==DocumentId &&  !x.Deleted);
        }
        public DocumentDto GetDocumentContentById(Guid contentId)
        {
            return context.DocumentData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.ContentId == contentId && !x.Deleted);
        }
        public DocumentDto GetDocumentContentLanguageById(Guid contentId, int LanguageId)
        {
            return context.DocumentData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.LanguageId==LanguageId && x.ContentId == contentId && !x.Deleted);

        }
        public List<DocumentDto> GetDocumentListByContentId(Guid contentId)
        {
            return context.DocumentData.All().Where(x => x.ContentId==contentId && x.CompanyId == CompanyContentId && !x.Deleted).ToList();
        }
        public void Insert(DocumentDto DocumentDto)
        {
            DocumentDto.CompanyId = CompanyContentId;
            context.DocumentData.Create(DocumentDto);
        }

        public void Update(DocumentDto DocumentDto)
        {
            DocumentDto.CompanyId = CompanyContentId;
            context.DocumentData.Update(DocumentDto);
        }

        public void Delete(DocumentDto DocumentDto)
        {
            context.DocumentData.Delete(DocumentDto); 
        }

       
    }
}
