﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class BlogService : IBlogService
    {
        private IDalContext context;

        public BlogService(IDalContext dal)
        {
            context = dal;
        }
        //private static int companyaId = UrlExtensions.SiteId;
        private static Guid CompanyContentId = UrlExtensions.SiteContentId;
        public List<BlogDto> BlogList()
        {
            return context.BlogData.All().Where(x=> x.CompanyId== CompanyContentId).ToList();
        }
        public List<BlogDto> BlogList(Guid contentId)
        {
            return context.BlogData.All().Where(x => x.CompanyId == CompanyContentId && x.ContentId== contentId).ToList();
        }
        public BlogDto GetBlogId(int blogId)
        {
            return context.BlogData.All().FirstOrDefault(x => x.Id == blogId && x.Deleted == false);
        }
        public BlogDto GetBlogContentId(Guid blogId)
        {
            return context.BlogData.All().FirstOrDefault(x => x.ContentId == blogId && x.Deleted == false);
        }
        public void Insert(BlogDto blog)
        {
            blog.CompanyId = CompanyContentId;
            context.BlogData.Create(blog);
        }

        public void Update(BlogDto blog)
        {
            blog.CompanyId = CompanyContentId;
            context.BlogData.Update(blog);
        }

        public void Delete(BlogDto blog)
        {
            blog.CompanyId = CompanyContentId;
            context.BlogData.Delete(blog);
        }

       
    }
}
