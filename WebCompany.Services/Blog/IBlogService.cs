﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IBlogService
    {
        List<BlogDto> BlogList();
        List<BlogDto> BlogList(Guid contentId);
        BlogDto GetBlogId(int blogId);
        BlogDto GetBlogContentId(Guid blogId);
        void Insert(BlogDto blog);
        void Update(BlogDto blog);
        void Delete(BlogDto blog);
    }
}
