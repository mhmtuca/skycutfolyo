﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class VideoService : IVideoService
    {
        private IDalContext context;

        public VideoService(IDalContext dal)
        {
            context = dal;
        }
 
        //private static int companyaId = UrlExtensions.SiteId;
        private static Guid CompanyContentId = UrlExtensions.SiteContentId;

        public List<VideoDto> GetVideoList()
        {
            return context.VideoData.All().Where(x=> x.CompanyId== CompanyContentId && !x.Deleted).ToList();
        }

        public VideoDto GetVideoById(int VideoId)
        {
            return context.VideoData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.Id==VideoId &&  !x.Deleted);
        }
        public VideoDto GetVideoContentById(Guid contentId)
        {
            return context.VideoData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.ContentId == contentId && !x.Deleted);
        }
        public VideoDto GetVideoContentLanguageById(Guid contentId, int LanguageId)
        {
            return context.VideoData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.LanguageId==LanguageId && x.ContentId == contentId && !x.Deleted);

        }
        public List<VideoDto> GetVideoListByContentId(Guid contentId)
        {
            return context.VideoData.All().Where(x => x.ContentId==contentId && x.CompanyId == CompanyContentId && !x.Deleted).ToList();
        }
        public void Insert(VideoDto VideoDto)
        {
            VideoDto.CompanyId = CompanyContentId;
            context.VideoData.Create(VideoDto);
        }

        public void Update(VideoDto VideoDto)
        {
            VideoDto.CompanyId = CompanyContentId;
            context.VideoData.Update(VideoDto);
        }

        public void Delete(VideoDto VideoDto)
        {
            context.VideoData.Delete(VideoDto); 
        }

       
    }
}
