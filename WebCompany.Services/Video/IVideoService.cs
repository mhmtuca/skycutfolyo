﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IVideoService
    {
        List<VideoDto> GetVideoList();
        List<VideoDto> GetVideoListByContentId(Guid contentId);
        VideoDto GetVideoById(int VideoId);
        VideoDto GetVideoContentById(Guid contentId);
        VideoDto GetVideoContentLanguageById(Guid contentId,int LanguageId);
        void Insert(VideoDto video);
        void Update(VideoDto video);
        void Delete(VideoDto video);
    }
}
