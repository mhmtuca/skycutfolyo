﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IMessageService
    {
        List<MessageDto> GetMessageList();
        MessageDto GetMessageId(int Id);  
        void Insert(MessageDto message);
        void Update(MessageDto message);
        void Delete(MessageDto message);
    }
}
