﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class MessageService : IMessageService
    {
        private IDalContext context;

        public MessageService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<MessageDto> GetMessageList()
        {
           return context.MessageData.All().Where(x => x.CompanyId == companyaId && x.Deleted == false).ToList();
        }

        public MessageDto GetMessageId(int Id)
        {
            return context.MessageData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Id==Id);
        }
        
        public void Insert(MessageDto message)
        {
            message.CompanyId = companyaId;
            context.MessageData.Create(message);
        }

        public void Update(MessageDto message)
        {
            message.CompanyId = companyaId;
            context.MessageData.Update(message);
        }

        public void Delete(MessageDto message)
        {
            message.CompanyId = companyaId;
            context.MessageData.Delete(message);
        }

       
    }
}
