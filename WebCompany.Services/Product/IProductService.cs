﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IProductService
    {
        List<ProductsDto> GetProductList();
        List<ProductsDto> GetProductList(Guid contentId);
        ProductsDto GetProductId(int productId); 
        ProductsDto GetProductGuidId(Guid ContentId); 
        void Insert(ProductsDto product);
        void Update(ProductsDto product);
        void Delete(ProductsDto product);
    }
}
