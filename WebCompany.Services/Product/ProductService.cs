﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class ProductService : IProductService
    {
        private IDalContext context;

        public ProductService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
        public List<ProductsDto> GetProductList()
        {
            var product= context.ProductData.All();
            if (product.Any())
            {
                return product.Where(x => x.CompanyId == companyaId && x.Deleted == false).OrderByDescending(x => x.Id).ToList();
            }
            return null;
        }

        public List<ProductsDto> GetProductList(Guid contentId)
        {
            var product = context.ProductData.All();
            if (product.Any())
            {
                return product.Where(x => x.ContentId== contentId & x.CompanyId == companyaId && x.Deleted == false).OrderByDescending(x => x.Id).ToList();
            }
            return null;
        }

        public ProductsDto GetProductId(int productId)
        {
            return context.ProductData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Id == productId && x.Deleted == false);
        }
        public ProductsDto GetProductGuidId(Guid ContentId)
        {
            return context.ProductData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.ContentId == ContentId  && x.Deleted == false);
        }
        public void Insert(ProductsDto product)
        {
            product.CompanyId = companyaId;
            context.ProductData.Create(product);
        }

        public void Update(ProductsDto product)
        {
            product.CompanyId = companyaId;
            context.ProductData.Update(product);
        }

        public void Delete(ProductsDto product)
        {
            product.CompanyId = companyaId;
            context.ProductData.Delete(product);
        }

       
    }
}
