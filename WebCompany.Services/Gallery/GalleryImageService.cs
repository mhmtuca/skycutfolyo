﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class GalleryImageService : IGalleryImageService
    {
        private IDalContext context;

        public GalleryImageService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
        public void Delete(GalleryImageDto GalleryImage)
        {
            GalleryImage.CompanyId = companyaId;
            context.GalleryImageData.Delete(GalleryImage);
        }

        public List<GalleryImageDto> GalleryImageList()
        {
            return context.GalleryImageData.All().Where(x => x.CompanyId == companyaId).ToList();
        }
        public List<GalleryImageDto> GalleryImageList(int GalleryId)
        {
            return context.GalleryImageData.All().Where(x => x.CompanyId == companyaId && x.GalleryId==GalleryId).ToList();
        }
        public GalleryImageDto GetGalleryImageById(int GalleryImageId)
        {
            return context.GalleryImageData.All().FirstOrDefault(x => x.Id == GalleryImageId && x.Deleted == false);
        }

        public void Insert(GalleryImageDto GalleryImage)
        {
            GalleryImage.CompanyId = companyaId;
            context.GalleryImageData.Create(GalleryImage);
        }

        public void Update(GalleryImageDto GalleryImage)
        {
            GalleryImage.CompanyId = companyaId;
            context.GalleryImageData.Update(GalleryImage);
        }

        
    }
}