﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class GalleryService : IGalleryService
    {
        private IDalContext context;

        public GalleryService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
        public void Delete(GalleryDto Gallery)
        {
            Gallery.CompanyId = companyaId;
            context.GalleryData.Delete(Gallery);
        }

        public List<GalleryDto> GalleryList()
        {
            return context.GalleryData.All().Where(x => x.CompanyId == companyaId).ToList();
        }

        public GalleryDto GetGalleryId(int GalleryId)
        {
            return context.GalleryData.All().FirstOrDefault(x => x.Id == GalleryId && x.Deleted == false);
        }

        public void Insert(GalleryDto Gallery)
        {
            Gallery.CompanyId = companyaId; 
            context.GalleryData.Create(Gallery);
        }

        public void Update(GalleryDto Gallery)
        {
            Gallery.CompanyId = companyaId;
            context.GalleryData.Update(Gallery);
        }

        public List<GalleryDto> GalleryListByContentId(Guid ContentId)
        {
            return context.GalleryData.All().Where(x => x.CompanyId == companyaId && x.ContentId==ContentId).ToList();
        }
    }
}