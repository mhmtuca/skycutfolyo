﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IGalleryService
    {
        List<GalleryDto> GalleryList();  
        GalleryDto GetGalleryId(int GalleryId);
        List<GalleryDto> GalleryListByContentId(Guid ContentId);
        void Insert(GalleryDto Gallery);
        void Update(GalleryDto Gallery);
        void Delete(GalleryDto Gallery);
    }
}
