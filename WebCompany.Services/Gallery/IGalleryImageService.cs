﻿using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IGalleryImageService
    {
        List<GalleryImageDto> GalleryImageList();
        GalleryImageDto GetGalleryImageById(int GalleryImageId);
        List<GalleryImageDto> GalleryImageList(int GalleryId);
        void Insert(GalleryImageDto GalleryImage);
        void Update(GalleryImageDto GalleryImage);
        void Delete(GalleryImageDto GalleryImage);
    }
}
