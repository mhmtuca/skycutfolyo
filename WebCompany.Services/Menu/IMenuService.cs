﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IMenuService
    {
        List<MenuDto> GetMenuList(); 
        MenuDto GetMenuIdFull(int Id);
        MenuDto GetMenuId(int Id);
        List<MenuDto> GetMenuTypeId(int Id);

        List<MenuDto> GetMenuContentById(Guid ContentId);
        MenuDto GetMenuPageId(int Id);
        void Insert(MenuDto menu);
        void Update(MenuDto menu);
        void Delete(MenuDto menu);
    }
}