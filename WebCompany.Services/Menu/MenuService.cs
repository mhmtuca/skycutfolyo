﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class MenuService : IMenuService
    {

        private IDalContext context;

        public MenuService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
        public void Delete(MenuDto menu)
        {
            menu.CompanyId = companyaId;
            context.MenuData.Delete(menu);
        }

        public MenuDto GetMenuId(int Id)
        {
            return context.MenuData.All().FirstOrDefault(x => x.Id == Id && x.CompanyId == companyaId && x.Deleted == false);
        }
        public List<MenuDto> GetMenuContentById(Guid ContentId)
        {
            return context.MenuData.All().Where(x => x.CompanyId == companyaId && x.Deleted == false).ToList();
        }
        public List<MenuDto> GetMenuList()
        {
            return context.MenuData.All().Where(x =>  x.CompanyId == companyaId && x.Deleted == false).ToList();
        }
        public MenuDto GetMenuIdFull(int Id)
        {
            return context.MenuData.All().FirstOrDefault(x => x.CompanyId == companyaId  );
        }
        public MenuDto GetMenuPageId(int Id)
        {
            return context.MenuData.All().FirstOrDefault(x =>  x.CompanyId == companyaId && x.Deleted == false);
        }

        public List<MenuDto> GetMenuTypeId(int Id)
        {
            return context.MenuData.All().Where(x =>   x.CompanyId == companyaId && x.Deleted==false).ToList();
        }

        public void Insert(MenuDto menu)
        {
            menu.CompanyId = companyaId;
            context.MenuData.Create(menu);
        }

        public void Update(MenuDto menu)
        {
            menu.CompanyId = companyaId;
            context.MenuData.Update(menu);
        }

       
    }
}
