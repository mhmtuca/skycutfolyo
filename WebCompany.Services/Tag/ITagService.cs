﻿using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ITagService
    {
        List<TagsDto> GetTagList();
        TagsDto GetTagId(int tagId); 
        void Insert(TagsDto TagDto);
        void Update(TagsDto TagDto);
        void Delete(TagsDto TagDto);
    }
}
