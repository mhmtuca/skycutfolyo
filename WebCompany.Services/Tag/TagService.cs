﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class TagService : ITagService
    {
        private IDalContext context;

        public TagService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<TagsDto> GetTagList()
        {
            return context.TagData.All().Where(x=> x.CompanyId==companyaId && x.Deleted==true).ToList();
        }

        public TagsDto GetTagId(int tagId)
        {
            return context.TagData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.ContentId==tagId && x.Deleted == true);
        }

        public void Insert(TagsDto TagDto)
        {
            context.TagData.Create(TagDto);
        }

        public void Update(TagsDto TagDto)
        {
            context.TagData.Update(TagDto);
        }

        public void Delete(TagsDto TagDto)
        {
            context.TagData.Delete(TagDto); 
        }
    }
}
