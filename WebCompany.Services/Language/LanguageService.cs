﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class LanguageService : ILanguageService
    {
        private IDalContext context;

        public LanguageService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;
        public void Delete(LanguageDto language)
        {
            context.LanguageData.Delete(language);
        }

        public LanguageDto GetLanguageById(int languageId)
        {
            return context.LanguageData.All().FirstOrDefault(x => x.Id == languageId && x.Deleted == false);
        }

        public List<LanguageDto> GetLanguageList()
        {
            return context.LanguageData.All().Where(x=>  x.Deleted == false).ToList();
        }
         

        public void Insert(LanguageDto language)
        { 
            context.LanguageData.Create(language);
        }

        public void Update(LanguageDto language)
        { 
            context.LanguageData.Update(language);
        }
    }
}
