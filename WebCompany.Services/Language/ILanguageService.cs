﻿using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ILanguageService
    {
        List<LanguageDto> GetLanguageList();
        LanguageDto GetLanguageById(int languageId); 
        void Insert(LanguageDto language);
        void Update(LanguageDto language);
        void Delete(LanguageDto language);
    }
}
