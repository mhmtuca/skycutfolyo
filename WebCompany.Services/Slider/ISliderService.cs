﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ISliderService
    {
        List<SliderDto> GetSliderList();
        List<SliderDto> GetSliderListByContentId(Guid ContentId);
        SliderDto GetSliderId(int sliderId); 
        void Insert(SliderDto SliderDto);
        void Update(SliderDto SliderDto);
        void Delete(SliderDto SliderDto);
    }
}
