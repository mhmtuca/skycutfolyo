﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class SliderService : ISliderService
    {
        private IDalContext context;

        public SliderService(IDalContext dal)
        {
            context = dal;
        }
        private static Guid companyaId = UrlExtensions.SiteContentId;

        public List<SliderDto> GetSliderList()
        {
            return context.SliderData.All().Where(x=> x.CompanyId==companyaId && x.Deleted==false).OrderBy(x=> x.Rows).ToList();
        }
        public List<SliderDto> GetSliderListByContentId(Guid ContentId)
        {
            return context.SliderData.All().Where(x => x.ContentId==ContentId &&  x.CompanyId == companyaId && x.Deleted == false).OrderBy(x => x.Rows).ToList();
        }
        public SliderDto GetSliderId(int sliderId)
        {
            return context.SliderData.All().FirstOrDefault(x => x.CompanyId == companyaId && x.Deleted == false && x.Id== sliderId);
        }

        public void Insert(SliderDto SliderDto)
        {
            SliderDto.CompanyId = companyaId;
            context.SliderData.Create(SliderDto);
        }

        public void Update(SliderDto SliderDto)
        {
            SliderDto.CompanyId = companyaId;
            context.SliderData.Update(SliderDto);
        }

        public void Delete(SliderDto SliderDto)
        {
            SliderDto.CompanyId = companyaId;
            context.SliderData.Delete(SliderDto);
        }

       
    }
}
