﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class CategoryService : ICategoryService
    {
        private IDalContext context;

        public CategoryService(IDalContext dal)
        {
            context = dal;
        }
        //private static int companyaId = UrlExtensions.SiteId;
        private static Guid CompanyContentId = UrlExtensions.SiteContentId;
        public void Delete(CategoryDto category)
        {
            context.CategoryData.Delete(category);
        }

        public CategoryDto GetCategoryId(int CategoryId)
        {
            return context.CategoryData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.Id == CategoryId && x.Deleted == false);
        }

        public List<CategoryDto> GetCategoryList()
        {
            return context.CategoryData.All().Where(x=> x.CompanyId == CompanyContentId && x.Deleted == false).ToList();
        }

        public List<CategoryDto> GetCategoryList(Guid ContentId)
        {
            return context.CategoryData.All().Where(x => x.ContentId== ContentId && x.CompanyId == CompanyContentId && x.Deleted == false).ToList();
        }

        public void Insert(CategoryDto category)
        {
            category.CompanyId = CompanyContentId;
            context.CategoryData.Create(category);
        }

        public void Update(CategoryDto category)
        {
            category.CompanyId = CompanyContentId;
            context.CategoryData.Update(category);
        }
    }
}
