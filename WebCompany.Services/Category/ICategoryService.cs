﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface ICategoryService
    {
        List<CategoryDto> GetCategoryList(); 
        List<CategoryDto> GetCategoryList(Guid ContentId);
        CategoryDto GetCategoryId(int CategoryId); 
        void Insert(CategoryDto category);
        void Update(CategoryDto category);
        void Delete(CategoryDto category);
    }
}
