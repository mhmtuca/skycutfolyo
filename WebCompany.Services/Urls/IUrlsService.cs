﻿using System;
using System.Collections.Generic;
using WebCompany.Data;

namespace WebCompany.Services
{
    public interface IUrlsService
    {
        List<UrlsDto> GetUrlList();
        List<UrlsDto> GetUrlListByContentId(Guid contentId);
        UrlsDto GetUrBylId(int UrlId);
        UrlsDto GetUrlContentById(Guid contentId);
        UrlsDto GetUrlContentLanguageById(Guid contentId,int LanguageId);
        void Insert(UrlsDto url);
        void Update(UrlsDto url);
        void Delete(UrlsDto url);
    }
}
