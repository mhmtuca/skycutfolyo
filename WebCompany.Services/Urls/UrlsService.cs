﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCompany.Core;
using WebCompany.Data;

namespace WebCompany.Services
{
    public class UrlsService : IUrlsService
    {
        private IDalContext context;

        public UrlsService(IDalContext dal)
        {
            context = dal;
        }
 
        //private static int companyaId = UrlExtensions.SiteId;
        private static Guid CompanyContentId = UrlExtensions.SiteContentId;

        public List<UrlsDto> GetUrlList()
        {
            return context.UrlData.All().Where(x=> x.CompanyId== CompanyContentId && !x.Deleted).ToList();
        }

        public UrlsDto GetUrBylId(int UrlId)
        {
            return context.UrlData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.Id==UrlId &&  !x.Deleted);
        }
        public UrlsDto GetUrlContentById(Guid contentId)
        {
            return context.UrlData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.ContentId == contentId && !x.Deleted);
        }
        public UrlsDto GetUrlContentLanguageById(Guid contentId, int LanguageId)
        {
            return context.UrlData.All().FirstOrDefault(x => x.CompanyId == CompanyContentId && x.LanguageId==LanguageId && x.ContentId == contentId && !x.Deleted);

        }
        public List<UrlsDto> GetUrlListByContentId(Guid contentId)
        {
            return context.UrlData.All().Where(x => x.ContentId==contentId && x.CompanyId == CompanyContentId && !x.Deleted).ToList();
        }
        public void Insert(UrlsDto UrlDto)
        {
            UrlDto.CompanyId = CompanyContentId;
            context.UrlData.Create(UrlDto);
        }

        public void Update(UrlsDto UrlDto)
        {
            UrlDto.CompanyId = CompanyContentId;
            context.UrlData.Update(UrlDto);
        }

        public void Delete(UrlsDto UrlDto)
        {
            context.UrlData.Delete(UrlDto); 
        }

       
    }
}
